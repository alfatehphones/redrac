redrac
======

`redrac` is a utility for sysadmins for doing out-of-band management
on systems. It uses the Redfish API to interface with remote management
controllers. Redfish is a standard, and in principle any device
(whether be it a computer system, storage box, or network appliance)
that implements Redfish, can be addressed with `redrac`.


Copyright and license
---------------------

Copyright 2023 by SURF bv

You may use the `redrac` software under terms described in the Apache
License Version 2.0 ("the License").

* http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


Features
--------

* `log` - Get system log
* `cfg` - Get system configuration / change hostname
* `hwinfo` - Get hardware information
* `fwinfo` - Get firmware versions
* `fwupdate` - Install firmware update
* `biosattr` - Manage BIOS settings
* `boot` - Manage boot order
* `network` - Manage network settings
* `cert` - Manage server certificate
* `passwd` - Change root/admin password
* `led` - Show or control indicator LED
* `thermals` - Control temperature readings and thresholds
* `fans` - Show fan status
* `psu` - Show PSU status
* `power` - Control power
* `racreset` - Restart of remote controller

Advanced features for more directly interfacing with Redfish objects:

* `path` - Access Redfish object via pseudo-path.
  Path may also be used to obtain Redfish URL
* `url` - Access Redfish object via direct URL
* `load` - Control Redfish objects via JSON command file(s).
  Useful for programming settings in JSON


Installation
------------

Use `flit` to install:

* `flit build`
* `flit install`

This installs the `redrac` command.


Configuration
-------------

`redrac` requires username and password to authenticate at the remote
management controller. It is convenient to configure this in a file.

*Warning*: The password will be stored in plaintext!

    # /etc/redrac.conf

    # defaults entry "Host *" applies to all
    Host *
        User root

    Host *.mgmt.surf.nl
        Password secret123

    Host mgmt2
        IPaddress 192.168.0.10
        User admin
        Password idunnowhat
        CAfile /etc/ssl/certs/ca-bundle.pem

    # EOB


Usage example: getting the hardware support identifier
------------------------------------------------------

Server systems usually come with hardware support, and have a support
identifier or support tag. You can see this support tag by issueing
the `cfg` command.

    $ redrac cfg serv-a1.mgmt.surf.nl
    Status:
    Health: OK
    HealthRollup: OK
    State: Enabled
    HostName: serv-a1
    PowerState: On
    Model: ThinkSystem SR650 V2
    Manufacturer: Lenovo
    SystemType: Physical
    PartNumber:
    None
    SerialNumber: A1234XYZ
    AssetTag:
    SKU: 1B12XYZ0AA
    ...

The exact support identifier is not specified in the Redfish standard.
Some vendors will use `SerialNumber`, others use the `SKU` field.


Usage example: power command
----------------------------

For example, we wish to powercycle a server system. This is done using
the `redrac power` command. The Redfish standard allows vendors to support
different kinds of 'power' actions, so the exact power command to give
may be different across hardware platforms. You can list the supported
power commands using `redrac power --list`:

    $ redrac power serv-a1.mgmt.surf.nl
    power: On

    $ redrac power -l serv-a1.mgmt.surf.nl
      ForceOff
      ForceOn
      ForceRestart
      GracefulRestart
      GracefulShutdown
      Nmi

On this particular system we can do `ForceRestart` or `GracefulRestart`.
Consult the manual of the system to find out the particular difference
between these actions.
Let's choose `GracefulRestart` (caps not required):

    $ redrac power -f gracefulrestart serv-a1.mgmt.surf.nl
    serv-a1.mgmt.surf.nl: ok

The `ok` response denotes that the command was successfully issued.
The `redrac` program does not wait for the server to come back up,
nor does it wait for the restart sequence to actually begin.


SSL certificates
----------------

### SSL connection
`redrac` makes an HTTPS connection to the remote management controller.
It does not verify the validity of the remote server certificate
unless you specify a CA file. The CA file may contain a single certificate,
or it may be a bundle containing a certificate chain.

### Server-side SSL certificate of the remote management controller
Use `redrac` to view or replace the server-side SSL certificate in the
remote management controller:

* the `cert` command shows the current certificate:

        $ redrac cert serv-a1.mgmt.surf.nl

* generate a CSR (certificate signing request):

        $ redrac cert --csr serv-a1.mgmt.surf.nl

  `redrac` will ask several questions about what this certificate is for,
  or you can feed it a CSR config file (see also: the `examples/` directory)

        $ redrac cert --csr -f csr_serv-a1_mgmt_surf_nl.cnf serv-a1.mgmt.surf.nl

  This creates a new private key in the remote management controller, plus
  a file named `serv-a1_mgmt_surf_nl.csr`. The CSR is to be signed by a CA
  (certificate authority). You will receive a public certificate file
  from the CA. The certificate is to be loaded with the `--import` switch.

  Note: the `--csr` switch is not suitable for making wildcard certificates.
  Use `openssl req` if you wish to obtain a wildcard certificate.

* `csr --import` loads a certificate file:

        $ redrac --import serv-a1_mgmt_surf_nl.cert serv-a1.mgmt.surf.nl

  The certificate file should be in PEM format.

  Note: If you used `redrac cert --csr` to obtain the certificate, then
  the certificate must match with the private key that is (already) stored
  in the remote management controller.
  If you used `openssl req` to obtain the certificate, then the file
  must contain both public and private key.


Factory reset
-------------

To reset the system to factory defaults, use these commands:

* `biosattr --factory-reset` : resets BIOS settings to factory defaults
* `racreset --factory-reset` : resets remote controller to factory defaults

Resetting BIOS settings typically requires a (manual) restart of the system.
Resetting the remote management controller typically automatically restarts
the remote management controller.
The exact behavior may differ between vendors.


Raw Redfish interface
---------------------

Most `redrac` commands support a `-j, --json` switch that prints the
designated Redfish object(s) in JSON format.

Additionally, `redrac` features three commands to support a more raw,
direct Redfish interface to your remote management controller.

* `path`
* `url`
* `load`

Redfish uses URLs to refer to objects. The exact URLs differ from vendor
to vendor, and may even be different between firmware versions.
While you certainly can use the 'hard' URL, it is more flexible to use
the `redrac` pseudo-path, for example:

```
    $ redrac path Managers:Members:EthernetInterfaces:Members serv-a1.mgmt
```

yields the URLs:

```
    /redfish/v1/Managers/1/EthernetInterfaces/NIC
    /redfish/v1/Managers/1/EthernetInterfaces/ToHost
```

A pseudo-path may expand to multiple URLs. It is important to address
the correct object, so we wish to narrow this down to just one URL.
In this particular example we are interested in the "NIC", and not in the
"ToHost" interface. We can filter by supplying a `path-match` parameter.
The "path-match" parameter is a key=value pair, that states that the object
must contain key, and have a matching value.
For the example of the manager's interface we can check that the "FQDN"
property is set (the "ToHost" interface has an empty FQDN).

```
    $ redrac path Managers:Members:EthernetInterfaces:Members \
        --match 'FQDN=serv-*.mgmt.*' serv-a1.mgmt
```

This yields only the URL:

```
    /redfish/v1/Managers/1/EthernetInterfaces/NIC
```

The `path` and `url` commands are particularly useful when writing JSON
command files. Such files can be loaded using the `load` command.
The `load` feature allows you to program any Redfish settings
even when `redrac` does not implement a command for it.

```
    {
      "comment": "change hostname",
      "redrac": true,
      "assert-vendor": "Lenovo*",
      "path": "Managers:Members:EthernetInterfaces:Members",
      "path-match": "FQDN=serv-*.mgmt*",
      "url": null,
      "request": "PATCH",
      "body": {
          "HostName": "$HOSTNAME"
      },
      "headers": {
          "If-Match": "*"
      }
    }
```

```
    $ redrac load cmd_set_hostname.json \
        -v HOSTNAME=myhost.domain.com serv-a1.mgmt
```

Some Redfish requests require an 'ETag' in the header. You may specify
header `"If-Match": "@etag"`, which will expand to the ETag value:

```
    {
      "comment": "change LED state",
      "redrac": true,
      "path": "Chassis:Members",
      "path-match": "LocationIndicatorActive=*",
      "request": "PATCH",
      "body": {
        "LocationIndicatorActive": "${LEDSTATE}"
      },
      "headers": {
        "If-Match": "@etag"
      }
    }
```

### `redrac` JSON specification

* `redrac` - boolean (required); must be `true`. Indicates that this object
  is for the `redrac` program
* `url` - string; URL to a Redfish object; may be `null`.
  The URL should be something like `/redfish/v1/...`
* `path` - string; `redrac` pseudo-path to a Redfish object; may be `null`.
  Either `url` or `path` must be set (`url` takes precedence)
* `path-match` - string; `KEY=PATTERN`; filters `path` down to a single URL.
  PATTERN may contain wildcards "*?" (like file globbing)
* `match-vendor` - string; system vendor or manufacturer must match PATTERN.
  If the vendor does not match, a warning message is issued, and `redrac`
  exits with exit status 0
* `assert-vendor` - string; system vendor or manufacturer must match PATTERN.
  If the vendor does not match, a warning message is issued, and `redrac`
  exits with exit status 255
* `request` - string (required); one of `GET|POST|PATCH|DELETE`;
  HTTP request to perform
* `body` - dictionary; HTTP request body. Contains Redfish keys and values
  appropriate for the request
* `headers` - dictionary; HTTP headers to pass.
  - Header `Content-Type: application/json` will be set automatically.
  - Header `If-Match` may contain special value `@etag`; it will be expanded
    to contain the ETag
* `comment` - not an official field; `redrac` ignores any unknown keys
  in the JSON object
* variable substitution: `$VAR` or `${VAR}` used in values will
  be substituted with the respective value as given on the command-line
  with option `-v, --assign`. Variables are not empty by default; not
  passing a required variable is an error


Remarks
-------
While Redfish is a standard, there exist differences between systems
that sometimes require workarounds. Vendors may or may not implement
everything in Redfish _as is_ according to the standard.
It is recommended to keep your management controller firmware up-to-date,
as vendors regularly release bugfixes and improvements to their Redfish
implementations.

`redrac` was developed and tested on these systems:

* Dell iDRAC
* Lenovo XClarity

(In particular, the `fwupdate` command / firmware update procedure for
Dell and Lenovo are different enough that `redrac` uses vendor-specific
implementations for this).

The `passwd` command may fail with an unclear error message. The culprit
is likely that your new password does not meet the security requirements
that are present in the controller. Vendors often implement extras on top
of the Redfish standard. The displayed error message is what the remote
management controller gives us. Please be aware of this.

#
#   redrac cmd_url.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: url

get/export/import Redfish object via URL
'''

import argparse
import json
import os

from typing import Dict, Any

from redrac import progress
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient
from redrac.util import print_data


def do_url(host: str, args: argparse.Namespace) -> None:
    '''get/export/import Redfish object via URL'''

    if args.export:
        args.json = True

    host, username, password, cafile = get_host(host, args).as_tuple()

    if args.import_file:
        # load settings from file
        with open(args.import_file, encoding='utf-8') as fh:
            try:
                settings = json.load(fh)

                # typecheck; the JSON data can be any kind of object
                if not isinstance(settings, dict):
                    short_filename = os.path.basename(args.import_file)
                    raise ValueError(f'{short_filename}: expected a Redfish object')

            except json.decoder.JSONDecodeError as err:
                short_filename = os.path.basename(args.import_file)
                raise ValueError(f'{short_filename}: {err}') from err

    progress.start()

    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        if args.import_file:
            if args.post:
                post_settings(redfish_obj, args.url, settings)
            else:
                patch_settings(redfish_obj, args.url, settings)
            return

        # show Redfish object
        data = redfish_obj.get(args.url)
        print_data(data, as_json=args.json)


def patch_settings(redfish_obj: RedfishClient, url: str, body: Dict[str, Any]) -> None:
    '''apply settings to Redfish URL via PATCH request
    May raise RedfishError, ValueError for invalid settings
    '''

    headers = {'If-Match': '*',
               'Content-Type': 'application/json'}
    redfish_obj.patch(url, body, headers)


def post_settings(redfish_obj: RedfishClient, url: str, body: Dict[str, Any]) -> None:
    '''apply settings to Redfish URL via POST request
    May raise RedfishError, ValueError for invalid settings
    '''

    headers = {'If-Match': '*',
               'Content-Type': 'application/json'}
    redfish_obj.post(url, body, headers)


# EOB

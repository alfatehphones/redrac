#
#   redrac cmd_boot.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: boot

Show or change boot order
'''

import argparse

from typing import List, Dict, Tuple, Any

from redrac import progress
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.redfish_util import get_vendor, is_vendor
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_boot(host: str, args: argparse.Namespace) -> None:
    '''show or change boot order'''

    progress.start()

    if args.set:
        change_boot_order(args.set, host, args)

    elif args.json:
        show_boot_data_json(host, args)

    else:
        boot_list = load_boot_order(host, args)
        show_boot_order(boot_list)


def show_boot_data_json(host: str, args: argparse.Namespace) -> None:
    '''load Redfish "Boot" section and show as JSON
    May raise RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        boot_data = redfish_boot_data(redfish_obj)

    print_data(boot_data, as_json=True)


def redfish_boot_data(redfish_obj: RedfishClient) -> Dict[str, Any]:
    '''Returns Redfish "Boot" section as dictionary
    May raise RedfishError
    '''

    data_list = redfish_obj.load('Systems:Members')
    for data in data_list:
        # use the first member that has Boot data
        # (typically there will be only one Systems member ... but who knows)
        try:
            return data['Boot']
        except KeyError:
            pass
    raise RedfishError("Redfish 'Boot' section not found")


def load_boot_order(host: str, args: argparse.Namespace) -> List[Tuple[str, str]]:
    '''load Redfish boot order data
    Returns list of tuples: [(boot_id, display_name), ]

    May raise RedfishError
    '''

    boot_list = []

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        boot_data = redfish_boot_data(redfish_obj)

        device_list = redfish_obj.load('Systems:Members:Boot:BootOptions:Members')

        try:
            for boot_id in boot_data['BootOrder']:
                for device in device_list:
                    try:
                        if device['Id'] == boot_id:
                            boot_list.append((boot_id, device['DisplayName']))
                            break
                    except KeyError:
                        pass

        except KeyError:
            pass

    return boot_list


def show_boot_order(boot_list: List[Tuple[str, str]]) -> None:
    '''display boot list
    The boot list are tuples: [(boot_id, display_name), ]
    '''

    if not boot_list:
        return

    width = max(len(x[0]) for x in boot_list)

    for boot_id, display_name in boot_list:
        print(f'{boot_id:<{width}}  {display_name}')


def change_boot_order(order: str, host: str, args: argparse.Namespace) -> None:
    '''change the boot order
    May raise RedfishError
    '''

    if not order:
        raise ValueError('usage: boot --set ORDER  (comma-separated list)')

    # split the boot order list and make sure there are no spaces there
    order_list = order.split(',')
    order_list = [x.strip() for x in order_list]

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            systems_url = redfish_obj.query_urls('Systems:Members')[0]
        except (KeyError, IndexError) as err:
            raise RedfishError('Redfish Systems URL not found') from err

        # the URL to change boot order may vary from vendor to vendor
        if is_vendor(get_vendor(redfish_obj), 'Lenovo'):
            change_boot_order_url = systems_url + '/Pending'
        else:
            change_boot_order_url = systems_url
        debug(f'change_boot_order_url == {change_boot_order_url}')

        body = {'Boot': {'BootOrder': order_list}}
        headers = {'If-Match': '*',
                   'Content-Type': 'application/json'}
        redfish_obj.patch(change_boot_order_url, body, headers)

    # ironically, a change in boot order requires a restart
    # otherwise you won't see the change
    print('note: system restart required')


# EOB

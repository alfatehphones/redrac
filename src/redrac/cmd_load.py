#
#   redrac cmd_load.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: load

import Redfish settings via JSON command file
'''

import os
import sys
import argparse
import json
import re
import fnmatch

from typing import List, Dict, Optional, Any

from redrac import progress
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.redfish_util import get_vendor, path_match_urls
from redrac.util import print_data, MultiLineError
from redrac.idict import idict
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_load(host: str, args: argparse.Namespace) -> None:
    '''load JSON command file'''

    if not args.files:
        raise ValueError('usage: load FILE [..]')

    # expand variables passed via cmdline
    variables = make_variables(args.assign)

    commands = load_command_files(args.files, variables)

    progress.start()

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        for command_dict in commands:
            response = perform_command(redfish_obj, command_dict)
            if response:
                print_data(response, as_json=True)


def load_command_files(files: List[str], variables: Dict[str, Any]) -> List[Dict[str, Any]]:
    '''load multiple JSON command files
    Returns list of command dictionaries
    '''

    commands = []

    for filename in files:
        command_dict = load_file(filename)
        command_dict = expand_variables(command_dict, variables)

        commands.append(command_dict)

    return commands


def load_file(filename: str) -> Dict[str, Any]:
    '''load JSON file
    Returns the JSON object as dictionary
    May raise OSError, ValueError, MultiLineError
    '''

    with open(filename, encoding='utf-8') as fh:
        try:
            command_dict = json.load(fh)
            check_command(command_dict)

        except json.decoder.JSONDecodeError as err:
            short_filename = os.path.basename(filename)
            raise ValueError(f'{short_filename}: {err}') from err

        except MultiLineError as err:
            # prepend the filename
            short_filename = os.path.basename(filename)
            err.messages = [short_filename + ': ' + x for x in err.messages]
            raise err

        except ValueError as err:
            short_filename = os.path.basename(filename)
            raise ValueError(f'{short_filename}: {err}') from err

    debug(f'command_dict == {command_dict!r}')
    return command_dict


def check_command(command_dict: Dict[str, Any]) -> None:
    '''perform various checks to ensure that the command makes sense
    May raise ValueError, MultiLineError
    '''

    # typecheck; the JSON data can be any kind of object
    try:
        assert isinstance(command_dict, dict)
    except AssertionError as err:
        raise ValueError('expected a dictionary object') from err

    errors = []

    try:
        assert command_dict['redrac'] is True
    except (KeyError, TypeError, AssertionError):
        errors.append('the JSON data must contain key: "redrac": true')

    try:
        has_path = command_dict['path'] is not None and command_dict['path'] != ''
    except KeyError:
        has_path = False

    try:
        has_url = command_dict['url'] is not None and command_dict['url'] != ''
    except KeyError:
        has_url = False

    if not has_path and not has_url:
        errors.append('the JSON data must contain key "path" or "url"')

    if has_path:
        try:
            assert isinstance(command_dict['path'], str)
        except AssertionError:
            errors.append('key "path" must be a string')

    if has_url:
        try:
            assert isinstance(command_dict['url'], str)
        except AssertionError:
            errors.append('key "url" must be a string')

    try:
        path_match = command_dict['path-match']
        assert isinstance(path_match, str)
        key, value = path_match.split('=', maxsplit=1)
        key = key.strip()
        if not key:
            raise ValueError('path-match: empty key')
        value = value.strip()
        if not value:
            raise ValueError('path-match: empty value')
    except KeyError:
        pass
    except AssertionError:
        errors.append('key "path-match" must be a string')
    except ValueError:
        errors.append('invalid "path-match"; syntax is KEY=VALUE')

    try:
        request = command_dict['request']
        assert isinstance(request, str)
        assert request.upper() in ('GET', 'POST', 'PATCH', 'DELETE')

    except KeyError:
        errors.append('the JSON data must contain key: "request"')

    except AssertionError:
        errors.append('key "request" must be one of: "GET", "POST", "PATCH", "DELETE"')

    try:
        body = command_dict['body']
        assert isinstance(body, dict)
    except KeyError:
        # a request without body is possible
        pass
    except AssertionError:
        errors.append('key "body" must be a dictionary')

    try:
        headers = command_dict['headers']
        assert headers is None or isinstance(headers, dict)
    except KeyError:
        # a request without explicit headers is possible
        pass
    except AssertionError:
        errors.append('key "headers" must be a dictionary')

    try:
        pattern = command_dict['match-vendor']
        assert isinstance(pattern, str)
    except KeyError:
        pass
    except AssertionError:
        errors.append('key "match-vendor" must be a string')

    try:
        pattern = command_dict['assert-vendor']
        assert isinstance(pattern, str)
    except KeyError:
        pass
    except AssertionError:
        errors.append('key "assert-vendor" must be a string')

    if errors:
        raise MultiLineError('there were errors', errors)


def make_variables(assign: Optional[List[str]]) -> Dict[str, Any]:
    '''convert list of assignments "key=value" to dictionary
    Returns dictionary
    May raise ValueError
    '''

    if assign is None:
        return {}

    d: Dict[str, Any] = {}

    for item in assign:
        try:
            key, value = item.split('=', maxsplit=1)
        except ValueError:
            d[item] = True
            continue

        key = key.strip()
        if not key:
            raise ValueError(f'invalid variable assignment: {item!r}')
        value = value.strip()
        if not value:
            d[key] = None
            continue

        # null value
        if value.lower() == 'null' or value.lower() == 'none':
            d[key] = None
            continue

        # boolean value
        if value.lower() == 'true':
            d[key] = True
            continue

        if value.lower() == 'false':
            d[key] = False
            continue

        # integer value
        try:
            # if it looks like an integer, it is an integer
            ivalue = int(value)
            d[key] = ivalue
        except ValueError:
            # string value
            d[key] = value

        # note: Redfish does not use floating point values;
        # they are usually strings

    return d


def expand_variables(data: Dict[str, Any], variables: Dict[str, Any]) -> Dict[str, Any]:
    '''expand any "$VAR" variables that are in dictionary `data`
    Returns a new dictionary with expanded values
    May raise KeyError when variable is not found
    '''

    d = {}
    for key, value in data.items():
        d[key] = expand_item(value, variables)
    return d


def expand_item(item: Any, variables: Dict[str, Any]) -> Any:
    '''expand any variables in `item`
    Returns expanded item
    May raise KeyError when variable is not found
    '''

    if isinstance(item, dict):
        return expand_variables(item, variables)

    if isinstance(item, list):
        return [expand_item(x, variables) for x in item]

    if isinstance(item, str):
        if '$' in item:
            return substitute_variable(item, variables)

    #else return item as-is
    return item


# a variable in a string is either "$VAR" or "${VAR}"
REGEX_VAR = re.compile(r'\$([A-Z_][A-Z0-9_]*|{[A-Z_][A-Z0-9_]*})')


def substitute_variable(item: str, variables: Dict[str, Any]) -> Any:
    '''substitute "$VAR" with value
    Raises KeyError if no such variable
    '''

    while True:
        m = REGEX_VAR.search(item)
        if not m:
            break

        name = m.group(1)
        matched = '$' + name
        if name.startswith('{') and name.endswith('}'):
            # strip accolades
            name = name[1:-1]
            if not name:
                continue

        try:
            value = variables[name]
            debug(f'substitute ${name} -> {value!r}')

            if item == matched:
                # value may be of type Any
                return value

            # do string substitution
            item = item.replace(matched, str(value))
        except KeyError as err:
            raise KeyError(f'variable ${name} is not defined') from err

    return item


def perform_command(redfish_obj: RedfishClient, command_dict: Dict[str, Any]) -> Dict[str, Any]:
    '''perform the command specified by `command_dict`
    May raise RedfishError, KeyError if Redfish URL not found or problem with ETag
    '''

    try:
        path = command_dict['path']
        if path is None:
            path = ''
    except KeyError:
        path = ''

    try:
        url = command_dict['url']
        if url is None:
            url = ''
    except KeyError:
        url = ''

    request = command_dict['request'].upper()

    try:
        body = command_dict['body']
    except KeyError:
        body = {}

    try:
        headers = command_dict['headers']
    except KeyError:
        headers = None

    try:
        pattern = command_dict['match-vendor']
        if not match_vendor(redfish_obj, pattern):
            print('warning: match-vendor: no match', file=sys.stderr)
            return {}
    except KeyError:
        pass

    try:
        pattern = command_dict['assert-vendor']
        assert_vendor(redfish_obj, pattern)
    except KeyError:
        pass

    if not url:
        # convert pseudo-path to URL
        try:
            path_match = command_dict['path-match']
        except KeyError:
            path_match = ''

        try:
            urls = redfish_obj.query_urls(path)
            if not urls:
                raise KeyError('no URL for path')
        except KeyError as err:
            raise RedfishError(f'invalid path: {path!r} ; Redfish URL not found') from err

        if not path_match:
            # by default pick the first URL
            url = urls[0]
        else:
            # select URL by path-match
            url = path_match_urls(redfish_obj, path_match, urls)

    # POST|PATCH requests may require an ETag
    headers = patch_etag_headers(redfish_obj, request, url, headers)

    data = perform_request(redfish_obj, request, url, body, headers)
    return data


def match_vendor(redfish_obj: RedfishClient, pattern: str) -> bool:
    '''Returns True if the vendor matches pattern
    May raise RedfishError
    '''

    vendor = get_vendor(redfish_obj)
    match = fnmatch.fnmatch(vendor, pattern)
    if match:
        debug(f'vendor {vendor!r} matches pattern {pattern!r}')
    else:
        match = fnmatch.fnmatch(vendor.lower(), pattern.lower())
        if match:
            debug(f'vendor {vendor!r} matches pattern {pattern!r} in lowercase')
        else:
            debug(f'vendor {vendor!r} does not match pattern {pattern!r}')
    return match


def assert_vendor(redfish_obj: RedfishClient, pattern: str) -> None:
    '''Exits the program with code 255 if the vendor does not match pattern
    May raise RedfishError
    '''

    if not match_vendor(redfish_obj, pattern):
        print('error: assert-vendor: mismatch', file=sys.stderr)
        sys.exit(255)


def patch_etag_headers(redfish_obj: RedfishClient, request: str, url: str, headers: Optional[Dict[str, Any]]) -> Optional[Dict[str, Any]]:
    '''finds the ETag for URL
    The ETag is only applicable for POST|PATCH requests
    and only if header "If-Match" equals magic "@etag"

    Returns new headers dictionary with "If-Match: <etag>"
    May raise RedfishError, KeyError if ETag not found
    '''

    if request not in ('POST', 'PATCH'):
        # ETag is only applicable for POST|PATCH requests
        return headers

    if headers is None:
        return None

    try:
        insensitive_headers = idict(headers)
        if insensitive_headers['If-Match'] != '@etag':
            return headers
    except KeyError:
        # no If-Match header set
        return headers

    try:
        etag = get_etag(redfish_obj, url)
    except KeyError as err:
        raise KeyError(f'If-Match: {err}') from err

    insensitive_headers['If-Match'] = etag
    return dict(insensitive_headers)


def get_etag(redfish_obj: RedfishClient, url: str) -> str:
    '''Returns ETag for URL
    May raise RedfishError, KeyError if ETag not found
    '''

    data, headers = redfish_obj.get_headers(url)
    try:
        etag = data['@odata.etag']
        debug(f'@odata.etag == {etag!r}')
        return etag
    except KeyError:
        pass

    try:
        insensitive_headers = idict(headers)
        etag = insensitive_headers['ETag']
        debug(f'ETag header == {etag!r}')
        return etag
    except KeyError:
        pass

    raise KeyError('ETag not found')


def perform_request(redfish_obj: RedfishClient, request: str, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]]) -> Dict[str, Any]:
    '''do the given request on the client
    Returns response data
    May raise RedfishError
    '''

    if request == 'GET':
        return redfish_obj.get(url)

    if request == 'POST':
        return redfish_obj.post(url, body, headers)

    if request == 'PATCH':
        return redfish_obj.patch(url, body, headers)

    if request == 'DELETE':
        redfish_obj.delete(url)
        return {}

    raise NotImplementedError(f'invalid request: {request!r}')


# EOB

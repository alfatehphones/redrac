#
#   redrac redfish_util.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''miscelleneous functions dealing with RedfishClient'''

import argparse
import fnmatch

from typing import List, Dict, Any

from redrac.debug import debug
from redrac.redfishcode import RedfishClient
from redrac.hostconf import get_host


def load(path: str, host: str, args: argparse.Namespace) -> List[Dict[str, Any]]:
    '''fetch Redfish data by pseudo-path

    pseudo-path is something like:
        "Chassis:Members:ThermalSubsystem:Fans:Members"

    Returns list of dictionaries (a list of Redfish objects)

    May raise RedfishError, KeyError for invalid path
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        return redfish_obj.load(path)


def strip_redfish_keys(data: Dict[str, Any]) -> Dict[str, Any]:
    '''remove Redfish keys from dict
    Returns dictionary without Redfish keys
    '''

    d = {}

    for key, value in data.items():
        if isinstance(value, dict):
            obj = strip_redfish_keys(value)
            if obj:
                d[key] = obj

        elif isinstance(value, list):
            arr = []
            for elem in value:
                if isinstance(elem, dict):
                    obj = strip_redfish_keys(elem)
                    if obj:
                        arr.append(obj)
                else:
                    arr.append(elem)
            if arr:
                d[key] = arr            # type: ignore

        else:
            try:
                if key[0] == '@' or '@Redfish.' in key or '@odata.' in key:
                    debug(f'deleted key {key!r}')
                    # NOTE rather than deleting it, we simply omit it from `d`
                else:
                    d[key] = value

            except IndexError:
                pass

    return d


def get_vendor(redfish_obj: RedfishClient) -> str:
    '''Returns vendor string
    May raise RedfishError, KeyError if vendor can not be found
    '''

    response = redfish_obj.get('/redfish/v1')
    try:
        vendor = response['Vendor']
        debug(f'vendor == {vendor!r}')
        return vendor
    except KeyError:
        pass

    # vendor not found; try Manufacturer
    try:
        urls = redfish_obj.query_urls('Systems:Members')
    except KeyError as err:
        raise KeyError('vendor not found') from err

    for url in urls:
        member = redfish_obj.get(url)
        try:
            manufacturer = member['Manufacturer']
            debug(f'manufacturer == {manufacturer!r}')
            return manufacturer

        except KeyError:
            pass

    raise KeyError('vendor not found')


def is_vendor(vendor: str, match_str: str) -> bool:
    '''Returns True if vendor  matches the match_str

    For example:
        if is_vendor(get_vendor(redfish_obj), 'DELL')
    '''

    return match_str.lower() in vendor.lower().split()


def path_match_urls(redfish_obj: RedfishClient, path_match: str, urls: List[str]) -> str:
    '''select URL from list of URLs by matching `path_match` with
    the Redfish objects behind those URLs
    The syntax of `path_match` is KEY=VALUE, where VALUE may contain wildcards

    Returns a selected URL
    May raise RedfishError,
    ValueError for invalid `path_match`,
    KeyError if no match is found
    '''

    try:
        key, pattern = path_match.split('=', maxsplit=1)
        key = key.strip()
        if not key:
            raise ValueError('path-match: empty key')
        pattern = pattern.strip()
        if not pattern:
            raise ValueError('path-match: empty pattern')
    except ValueError as err:
        raise ValueError('invalid "path-match"; syntax is KEY=VALUE') from err

    for url in urls:
        data = redfish_obj.get(url)
        try:
            if fnmatch.fnmatch(data[key], pattern):
                debug(f'property {key}: {data[key]!r} matches {pattern!r}')
                return url

        except TypeError:
            # data[key] is not a string
            if pattern == '*' or str(data[key]) == pattern:
                debug(f'property {key}: {data[key]!r} matches {pattern!r}')
                return url

        except KeyError:
            pass

    debug(f'no matching URL found for path-match: {key}={pattern!r}')
    raise KeyError(f'no matching Redfish URL found for path-match: {key}={pattern!r}')


# EOB

#
#   redrac cmd_network.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: network

Show or change network settings of the remote management controller
'''

import argparse
import json
import os

from typing import Dict, Any

from redrac import progress
from redrac import redfish_util
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.util import print_dict, print_data


def do_network(host: str, args: argparse.Namespace) -> None:
    '''show or change network settings'''

    if args.import_file:
        import_network_settings(args.import_file, host, args)
        return

    if args.export:
        # exporting is the same as printing all settings as JSON
        args.json = True

    show_network_settings(host, args)


def show_network_settings(host: str, args: argparse.Namespace) -> None:
    '''show network settings'''

    data = load_network(host, args)
    if args.json:
        print_data(data, as_json=True)
    else:
        data = redfish_util.strip_redfish_keys(data)
        print_dict(data, as_json=False)


def load_network(host: str, args: argparse.Namespace) -> Dict[str, Any]:
    '''Returns dictionary with Redfish data with network settings
    May raise RedfishError
    '''

    data_list = redfish_util.load('Managers:Members:EthernetInterfaces:Members', host, args)
    try:
        return data_list[0]
    except IndexError as err:
        raise RedfishError('Redfish Managers EthernetInterfaces not found') from err


def import_network_settings(filename: str, host: str, args: argparse.Namespace) -> None:
    '''import network settings from file
    May raise OSError, RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()

    with open(filename, encoding='utf-8') as fh:
        try:
            settings = json.load(fh)
        except json.decoder.JSONDecodeError as err:
            short_filename = os.path.basename(filename)
            raise ValueError(f'{short_filename}: {err}') from err

    progress.start()

    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        set_network_settings(redfish_obj, settings)


def set_network_settings(redfish_obj: RedfishClient, settings: Dict[str, Any]) -> None:
    '''change network settings via Redfish
    May raise RedfishError
    '''

    # if Dell than different; they use a URL with /Attributes
    if redfish_util.is_vendor(redfish_util.get_vendor(redfish_obj), 'DELL'):
        url = dell_network_settings_url(redfish_obj)
        body = {'Attributes': settings}
    else:
        url = find_network_settings_url(redfish_obj)
        body = settings

    headers = {'If-Match': '*',
               'Content-Type': 'application/json'}
    redfish_obj.patch(url, body, headers)


def dell_network_settings_url(redfish_obj: RedfishClient) -> str:
    '''Returns URL to change network settings for DELL iDRAC
    DELL uses the Managers URL + '/Attributes'

    May raise RedfishError
    '''

    try:
        manager_url = redfish_obj.query_urls('Managers:Members')[0]
        url = manager_url + '/Attributes'
        debug(f'url == {url}')
        return url

    except (KeyError, IndexError) as err:
        raise RedfishError('no Redfish Manager found') from err


def find_network_settings_url(redfish_obj: RedfishClient) -> str:
    '''Returns URL to change network settings
    May raise RedfishError
    '''

    # the returned URL is the 1st NIC of the 1st Manager

    try:
        url = redfish_obj.query_urls('Managers:Members:EthernetInterfaces:Members')[0]
        debug(f'url == {url}')
        return url

    except (KeyError, IndexError) as err:
        raise RedfishError('Redfish Managers EthernetInterfaces not found') from err


# EOB

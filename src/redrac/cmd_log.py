#
#   redrac cmd_log.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: log

Shows the remote management controller log, which should contain
system hardware events.
You may also clear the system logs
'''

import argparse
import datetime

from typing import List, Dict, Any

from redrac import progress
from redrac.redfishcode import RedfishClient
from redrac.hostconf import get_host
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_log(host: str, args: argparse.Namespace) -> None:
    '''get system log'''

    progress.start()

    if args.clear:
        clear_logs(host, args)
        return

    log_entries = load_logs(host, args)
    if args.json:
        print_data(log_entries, as_json=True)
        return

    # print log entries
    for entry in log_entries:
        if args.sel:
            try:
                # show only Dell SEL (system event log)
                entrytype = entry['EntryType']
                if entrytype != 'SEL':
                    continue

            except KeyError:
                continue

        if args.lc:
            # show only Dell lclog (lifecycle controller log)
            try:
                entrytype = entry['EntryType']
                recordformat = entry['OemRecordFormat']
                if not (entrytype == 'Oem' and recordformat == 'Dell'):
                    continue

                if 'DellLCLogEntry' not in entry['Oem']['Dell']['@odata.type']:
                    continue

            except KeyError:
                continue

        timestamp = entry['Created']
        severity = entry['Severity']
        message = entry['Message']
        print(f'{timestamp} {severity} {message}')


def load_logs(host: str, args: argparse.Namespace) -> List[Dict[str, Any]]:
    '''fetch system logs
    Returns list of Redfish log entries
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        # logs may occur under various sections
        urls = []
        for section in ('Systems', 'Chassis', 'Managers'):
            path = section + ':Members:LogServices:Members:Entries'
            urls.extend(redfish_obj.query_urls(path))
        # filter double urls
        urls = list(dict.fromkeys(urls))

        # get() all the URLs
        # the individual log entries are under the key 'Members'
        log_entries = []
        for url in urls:
            data = redfish_obj.get(url)
            try:
                log_entries.extend(data['Members'])
            except KeyError:
                pass

    return sort_by_timestamp(log_entries)


def sort_by_timestamp(log_entries: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    '''sort list of log entries by timestamp
    Returns sorted list of log entries
    '''

    # sort log entries by timestamp
    pairs = []
    for entry in log_entries:
        try:
            pairs.append((parse_timestamp(entry['Created']), entry))
        except KeyError:
            # no timestamp; treat as invalid entry; ignore
            pass
    sorted_pairs = sorted(pairs, key=lambda x: x[0])

    # return list of log entries
    return [x[1] for x in sorted_pairs]


def parse_timestamp(timestamp: str) -> datetime.datetime:
    '''parse timestamp from log entry
    Returns datetime instance
    May raise ValueError for an invalid timestamp

    The problem with timestamps is that Python does not correctly
    parse all ISO 8601 formats

    This function parses:
        "2020-02-03T09:56:27+01:00"
        "2023-03-23T15:14:54.021+00:00"
        "2023-03-23T13:12:10Z"
    '''

    try:
        date_str = timestamp[:10]
        time_str = timestamp[11:19]
        trailer = timestamp[19:]

        if trailer and trailer[-1] == 'Z':
            trailer = trailer[:-1]
            timezone = 'Z'
        else:
            timezone = ''

        if '+' in trailer:
            usec_str, timezone = trailer.split('+', maxsplit=1)
            trailer = ''
        elif trailer and trailer[0] == '.':
            usec_str = trailer
            trailer = ''
        else:
            usec_str = ''
        if trailer:
            raise ValueError(f'invalid timestamp: {timestamp!r}')

        if not usec_str:
            usec_str = '.000000'
        # pad with zeroes to 6 digits
        usec_str = usec_str.ljust(7, '0')

        if not timezone or timezone == 'Z':
            timezone = '0000'
        elif ':' in timezone:
            # remove colon from timezone
            timezone = timezone.replace(':', '')

        t = date_str + 'T' + time_str + usec_str + '+' + timezone
        return datetime.datetime.strptime(t, '%Y-%m-%dT%H:%M:%S.%f%z')

    except IndexError as err:
        raise ValueError(f'invalid timestamp: {timestamp!r}') from err


def clear_logs(host: str, args: argparse.Namespace) -> None:
    '''clear all logs'''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        urls = []
        for section in ('Systems', 'Chassis', 'Managers'):
            path = section + ':Members:LogServices:Members'
            urls.extend(redfish_obj.query_urls(path))
        # filter double urls
        urls = list(dict.fromkeys(urls))

        for log_url in urls:
            log_response = redfish_obj.get(log_url)

            # the target is the URL of where to send the POST request to clear the log
            try:
                target_url = log_response['Actions']['#LogService.ClearLog']['target']
            except KeyError:
                # this log service has no action to clear it somehow
                continue

            body = {}                   # type: Dict[str, Any]
            try:
                # the clear action may be some word defined as 'AllowableValue'
                # use the first AllowableValue as action (if it exists)
                actioninfo_url = log_response['Actions']['#LogService.ClearLog']['@Redfish.ActionInfo']
                actioninfo = redfish_obj.get(actioninfo_url)
                params = actioninfo['Parameters']
                for param in params:
                    try:
                        body[param['Name']] = param['AllowableValues'][0]
                        break
                    except KeyError:
                        continue

            except KeyError:
                # there is no action info; use the default action
                # (this is a rather normal condition)
                body = {'Action': 'LogService.ClearLog'}

            # show message
            try:
                if log_url.endswith('/'):
                    log_name = log_url.split('/')[-2]
                else:
                    log_name = log_url.split('/')[-1]
            except (IndexError, ValueError):
                log_name = 'log'
            print(f'clearing {log_name}')

            # perform the clear action
            redfish_obj.post(target_url, body)


# EOB

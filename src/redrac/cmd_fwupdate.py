#
#   redrac cmd_fwupdate.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: fwupdate'''

import os
import argparse
import http
import json

from typing import List

import requests

from redrac import progress
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError, RedfishTask
from redrac.redfish_util import get_vendor, is_vendor
from redrac.idict import idict
from redrac.print import print                                      # pylint: disable=redefined-builtin


HUGE_TIMEOUT = 20 * 60  # seconds


def do_fwupdate(host: str, args: argparse.Namespace) -> None:
    '''do firmware update

    args.file is firmware file
    args.target is target (may be empty string)
    args.clearlock is True when locks need to be forcibly cleared
    '''

    if not args.file and not args.clearlock:
        raise ValueError('usage: fwupdate [-f FILE [-t TARGET]] [--clearlock]')

    progress.start()

    if args.file:
        debug(f'args.file == {args.file!r}')
        debug(f'args.target == {args.target!r}')

        host, username, password, cafile = get_host(host, args).as_tuple()
        firmware_update(host, username, password, cafile, args.file, args.target, args)

        print('firmware loaded; restart system to upgrade')

    if args.clearlock:
        # forcibly clear any locks set by a previous botched firmware update
        firmware_clearlock(host, args)


def firmware_update(host: str, username: str, password: str, cafile: str, filename: str, target: str, args: argparse.Namespace) -> None:
    '''install firmware update

    target may be empty string, for default target

    May raise RedfishError, OSError
    '''

    # although there is a Redfish standard, in practice each vendor
    # does their own thing when it comes to firmware updates
    # Therefore we 'detect' what kind of system we are dealing with,
    # and proceed with vendor-specific procedure

    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        vendor = get_vendor(redfish_obj)
        if is_vendor(vendor, 'DELL'):
            dell_firmware_update(redfish_obj, password, cafile, filename, target, args)

        elif is_vendor(vendor, 'Lenovo'):
            lenovo_firmware_update(redfish_obj, password, cafile, filename, target, args)

        else:
            raise NotImplementedError(f'firmware update not implemented for system: {vendor}')


def dell_firmware_update(redfish_obj: RedfishClient, password: str, cafile: str, filename: str, target: str, _args: argparse.Namespace) -> None:
    '''perform firmware update on a DELL system
    May raise RedfishError
    '''

    try:
        push_url = redfish_obj.query_urls('UpdateService:HttpPushUri')[0]
        debug(f'push_url == {push_url}')
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to get Redfish HttpPushUri') from err

    try:
        firmware_inv_url = redfish_obj.query_urls('UpdateService:FirmwareInventory')[0]
        debug(f'firmware_inv_url == {firmware_inv_url}')
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to get Redfish firmware inventory URL') from err

    _firmware_inv_response, headers = redfish_obj.get_headers(firmware_inv_url)

    if target and '/' not in target:
        target = firmware_inv_url + '/' + target
    debug(f'target == {target}')

    # DELL requires header with "ETag"
    try:
        insensitive_headers = idict(headers)
        etag = insensitive_headers['ETag']
        debug(f'ETag == {etag}')
        headers = {'If-Match': etag}
    except KeyError:
        headers = {}

    # step 1 : upload the firmware file

    with open(filename, 'rb') as fh:
        short_filename = os.path.basename(filename)
        params = {'file': (short_filename, fh, 'multipart/form-data')}
        auth = requests.auth.HTTPBasicAuth(redfish_obj.username, password)
        if cafile:
            verify = cafile
        else:
            verify = False              # type: ignore
        print(f'loading {short_filename} ...')
        debug(f'POST {firmware_inv_url} files={params!r} headers={headers!r}')
        firmware_upload_url = 'https://' + redfish_obj.host + firmware_inv_url
        firmware_upload_response = requests.post(firmware_upload_url, auth=auth, verify=verify, files=params,   # type: ignore
                                                 headers=headers, timeout=HUGE_TIMEOUT)
        http_code = redfish_obj.format_http_status(firmware_upload_response.status_code)
        debug(f'firmware_upload_response.status_code == {http_code}')
        firmware_upload_response_dict = firmware_upload_response.json()
        debug(f'firmware_upload_response_dict == {firmware_upload_response_dict!r}')

        # print messages (if any)
        msg = RedfishClient.get_message(firmware_upload_response_dict).rstrip()
        if msg:
            print(msg)

    if firmware_upload_response.status_code in (http.HTTPStatus.OK, http.HTTPStatus.CREATED, http.HTTPStatus.NO_CONTENT):
        # completed OK
        pass
    elif firmware_upload_response.status_code == http.HTTPStatus.ACCEPTED:
        # HTTP 202 Accepted; asynchronous task in server
        try:
            task_url = firmware_upload_response_dict['@odata.id']
            debug(f'task_url == {task_url}')
        except KeyError as err:
            raise RedfishError('invalid response from server (task URL not found)') from err

        task = RedfishTask(task_url, redfish_obj)
        task.monitor()
    else:
        http_code = redfish_obj.format_http_status(firmware_upload_response.status_code)
        msg = RedfishClient.get_extended_error(firmware_upload_response_dict).rstrip()
        if msg:
            msg = '\n' + msg
        raise RedfishError(f'{http_code} - firmware upload failed{msg}')

    # step 2 : get ImageURI to newly available firmware
    try:
        available = firmware_upload_response_dict['Id']
    except KeyError as err:
        raise RedfishError('ImageURI to available uploaded firmware not found') from err

    # step 3 : install firmware via POST to SimpleUpdate

    try:
        install_url = redfish_obj.query_urls('UpdateService:Actions:#UpdateService.SimpleUpdate:target')[0]
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to get Redfish firmware install URL') from err

    body = {'ImageURI': push_url + '/' + available}
    try:
        update_svc_response = redfish_obj.load('UpdateService')[0]
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to load Redfish UpdateService') from err
    try:
        # firmware will be activated on restart of the system
        if 'OnRestart' in update_svc_response['Actions']['#UpdateService.SimpleUpdate']['@Redfish.OperationApplyTimeSupport']['SupportedValues']:
            body['@Redfish.OperationApplyTime'] = 'OnRestart'
    except KeyError:
        pass

    install_response = redfish_obj.post(install_url, body)

    # print response message (if any)
    msg = redfish_obj.get_message(install_response).rstrip()
    if msg:
        print(msg)

    # we do not restart the system here; manual restart required


def lenovo_firmware_update(redfish_obj: RedfishClient, password: str, cafile: str, filename: str, target: str, _args: argparse.Namespace) -> None:
    '''perform firmware update on Lenovo system
    May raise RedfishError
    '''

    try:
        update_svc_url = redfish_obj.query_urls('UpdateService')[0]
        debug(f'update_svc_url == {update_svc_url}')
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to get Redfish UpdateService URL') from err

    try:
        push_url = redfish_obj.query_urls('UpdateService:MultipartHttpPushUri')[0]
        debug(f'push_url == {push_url}')
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to get Redfish MultipartHttpPushUri') from err

    if target and '/' not in target:
        try:
            firmware_inv_url = redfish_obj.query_urls('UpdateService:FirmwareInventory')[0]
            debug(f'firmware_inv_url == {firmware_inv_url}')
        except (KeyError, IndexError) as err:
            raise RedfishError('failed to get Redfish firmware inventory URL') from err

        target = firmware_inv_url + '/' + target
    debug(f'target == {target}')

    update_svc_response = redfish_obj.get(update_svc_url)

    with open(filename, 'rb') as fh:
        # TargetsBusy is present on Lenovo XClarity
        # this is a soft-locking mechanism
        patch_targets_busy = 'HttpPushUriTargetsBusy' in update_svc_response
        debug(f'patch_targets_busy == {patch_targets_busy}')

        if patch_targets_busy:
            if update_svc_response['HttpPushUriTargetsBusy']:
                # the target is busy, bail out
                raise RedfishError('target is busy')

            redfish_obj.patch(update_svc_url, {'HttpPushUriTargetsBusy': True})

        # BMC/BMC-Backup update procedure on Lenovo XClarity
        # requires patching HttpPushUriTargets and HttpPushUriTargetsBusy
        patch_targets = os.path.basename(target) in ('BMC', 'BMC-Backup')
        if patch_targets:
            redfish_obj.patch(update_svc_url, {'HttpPushUriTargets': [target, ]})

        # perform multipart push of file using requests.post()
        auth = requests.auth.HTTPBasicAuth(redfish_obj.username, password)
        if cafile:
            verify = cafile
        else:
            verify = False              # type: ignore
        target_list: List[str] = []
        if target:
            target_list.append(target)
        short_filename = os.path.basename(filename)
        params = {'UpdateParameters': (target, json.dumps({'Targets': target_list}), 'application/json'),
                  'UpdateFile': (short_filename, fh, 'application/octet-stream')}
        firmware_update_url = 'https://' + redfish_obj.host + push_url

        print(f'loading {short_filename} ...')
        debug(f'POST {push_url} files={params!r}')
        firmware_update_response = requests.post(firmware_update_url, auth=auth, verify=verify, files=params,       # type: ignore
                                                 timeout=HUGE_TIMEOUT)
        http_code = redfish_obj.format_http_status(firmware_update_response.status_code)
        debug(f'firmware_update_response.status_code == {http_code}')
        firmware_update_response_dict = firmware_update_response.json()
        debug(f'firmware_update_response_dict == {firmware_update_response_dict!r}')

        # print messages (if any)
        msg = RedfishClient.get_message(firmware_update_response_dict).rstrip()
        if msg:
            print(msg)

    err_msg = ''
    if firmware_update_response.status_code in (http.HTTPStatus.OK, http.HTTPStatus.NO_CONTENT):
        # completed OK
        pass
    elif firmware_update_response.status_code == http.HTTPStatus.ACCEPTED:
        # HTTP 202 Accepted; asynchronous task in server
        try:
            task_url = firmware_update_response_dict['@odata.id']
            debug(f'task_url == {task_url}')
        except KeyError as err:
            raise RedfishError('invalid response from server (task URL not found)') from err

        task = RedfishTask(task_url, redfish_obj)
        task.monitor()
    else:
        err_msg = f'{http_code} - firmware update failed'
        msg = RedfishClient.get_extended_error(firmware_update_response_dict).rstrip()
        if msg:
            err_msg += '\n' + msg

    # clear serverside patches
    if patch_targets:
        redfish_obj.patch(update_svc_url, {'HttpPushUriTargets': []})

    if patch_targets_busy:
        redfish_obj.patch(update_svc_url, {'HttpPushUriTargetsBusy': False})

    # finally; if the update failed, raise an error
    if err_msg:
        raise RedfishError(err_msg)


def firmware_clearlock(host: str, args: argparse.Namespace) -> None:
    '''force clear busy status of firmware updates

    TargetsBusy is present on Lenovo XClarity
    this is a soft-locking mechanism that may erroneously stay in a busy state
    This function clears the lock
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            update_svc_url = redfish_obj.query_urls('UpdateService')[0]
            debug(f'update_svc_url == {update_svc_url}')
        except (KeyError, IndexError) as err:
            raise RedfishError('failed to get Redfish UpdateService URL') from err

        update_svc_response = redfish_obj.get(update_svc_url)

        # clear serverside patches
        if 'HttpPushUriTargets' in update_svc_response:
            redfish_obj.patch(update_svc_url, {'HttpPushUriTargets': []})

        if 'HttpPushUriTargetsBusy' in update_svc_response:
            redfish_obj.patch(update_svc_url, {'HttpPushUriTargetsBusy': False})

    print('ok')


# EOB

#
#   redrac util.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''miscellaneous functions'''

import json

from typing import List, Dict, Union, Any

from redrac.print import print                                      # pylint: disable=redefined-builtin


class MultiLineError(ValueError):
    '''exception that has a multiple messages'''

    def __init__(self, short: str, messages: List[str]) -> None:
        '''initialize instance'''

        super().__init__(short)
        self.messages = messages

    def __str__(self) -> str:
        '''Returns string'''

        # note, we do not display `self.message`; the `short` is only a placeholder
        return '\n'.join(self.messages)



def print_item(item: Any, indent: int = 0) -> None:
    '''print a value'''

    if isinstance(item, dict):
        for k, v in item.items():
            if indent:
                print(' ' * indent, end='')

            if isinstance(v, (str, int, bool)):
                print(f'{k}: {v}')
            else:
                print(f'{k}:')
                print_item(v, indent=indent + 2)

    elif isinstance(item, tuple):
        if indent:
            print(' ' * indent, end='')

        if len(item) == 2:
            k, v = item
            if isinstance(v, (str, int, bool)):
                print(f'{k}: {v}')
            else:
                print(f'{k}:')
                print_item(v, indent=indent + 2)
        else:
            print_item(list(item), indent)

    elif isinstance(item, list):
        for v in item:
            print_item(v, indent)

    else:
        if indent:
            print(' ' * indent, end='')
        print(f'{item}')


def print_dict(d: Dict[str, Any], as_json: bool = False) -> None:
    '''print dictionary to screen'''

    if as_json:
        print(json.dumps(d, indent=2))
    else:
        print_item(d)


def print_dictlist(data_list: List[Dict[str, Any]], as_json: bool = False) -> None:
    '''print list of dictionaries to screen'''

    if as_json:
        print(json.dumps(data_list, indent=2))
    else:
        for data in data_list:
            print_dict(data)


def print_data(data: Union[Dict[str, Any], List[Dict[str, Any]]], as_json: bool = False) -> None:
    '''print data to screen
    data is either a dictionary or a list of dictionaries
    '''

    # list containing only a single item
    if isinstance(data, list) and len(data) == 1:
        data = data[0]

    if as_json:
        print(json.dumps(data, indent=2))
    else:
        print_item(data)


# EOB

#
#   redrac redfishcode.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''redfish wrapper classes'''

import http
import json
import time

from typing import List, Dict, Tuple, Optional, Any

import redfish

from redrac.debug import debug
from redrac.print import print                                      # pylint: disable=redefined-builtin


class RedfishError(Exception):
    '''Redfish related error'''



class RedfishClient:
    '''represents a Redfish client'''

    def __init__(self) -> None:
        '''initialize instance'''

        self.host = ''
        self.username = ''
        self.redfish_obj = None             # type: Optional[redfish.rest.v1.HttpClient]

    def __enter__(self) -> 'RedfishClient':
        '''enter managed context'''

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback) -> None:
        '''leave managed context'''

        if self.redfish_obj is not None:
            self.logout()

    @staticmethod
    def login(host: str, username: str, password: str, cafile: str = '') -> 'RedfishClient':
        '''login to remote controller
        Returns a RedfishClient instance
        that can be used as a context manager:

            with RedfishClient.login(host, username, password) as redfish_obj:
                ...
        '''

        if not cafile:
            cafile = None               # type: ignore

        obj = RedfishClient()
        obj.redfish_obj = redfish.redfish_client(base_url='https://' + host, username=username, password=password,
                                                 default_prefix='/redfish/v1', timeout=10, cafile=cafile)
        obj.redfish_obj.login(auth='session')
        obj.host = host
        obj.username = username
        return obj

    def logout(self) -> None:
        '''logout of session'''

        assert self.redfish_obj is not None

        self.redfish_obj.logout()
        self.host = ''
        self.username = ''
        self.redfish_obj = None

    @staticmethod
    def format_json(d: Dict[str, Any]) -> str:
        '''Returns pretty-printed JSON string'''

        return json.dumps(d, indent=2)

    @staticmethod
    def format_http_status(code: int) -> str:
        '''format HTTP status code as text
        Returns text string
        '''

        return f'{code} {http.HTTPStatus(code).phrase}'

    @staticmethod
    def make_response_dict(response: redfish.rest.v1.RestResponse) -> Dict[str, Any]:
        '''Returns dictionary of response body'''

        # this kludge is to prevent getting an invalid JSON error message
        # from within the Redfish module
        response_dict = {}
        if response.status != http.HTTPStatus.NO_CONTENT:
            try:
                response_dict = response.dict
            except redfish.rest.v1.JsonDecodingError:
                debug('caught JsonDecodingError; setting empty response_dict')
                response_dict = {}

        return response_dict

    def get(self, url: str) -> Dict[str, Any]:
        '''do a Redfish GET request
        Returns dictionary with response data
        May raise RedfishError
        '''

        assert self.redfish_obj is not None

        debug(f'GET {url}')
        response = self.redfish_obj.get(url)
        debug(f'response.status == {self.format_http_status(response.status)}')
        if response.status != http.HTTPStatus.OK:
            raise RedfishError(f'{self.format_http_status(response.status)} GET {url}')

        response_dict = self.make_response_dict(response)
        debug(f'response_dict == {response_dict!r}')

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict

    def get_headers(self, url: str) -> Tuple[Dict[str, Any], Dict[str, Any]]:
        '''do a Redfish GET request
        Returns tuple: (dictionary with response data, dictionary with headers)
        May raise RedfishError
        '''

        # note, HTTP headers are officially case-insensitive
        # be aware that we return the headers in a case-sensitive Python dictionary
        # (which is quite common practice when dealing with HTTP in Python)

        assert self.redfish_obj is not None

        debug(f'GET {url}')
        response = self.redfish_obj.get(url)
        debug(f'response.status == {self.format_http_status(response.status)}')
        if response.status != http.HTTPStatus.OK:
            raise RedfishError(f'{self.format_http_status(response.status)} GET {url}')

        # response.getheaders() returns list of (key, value) pairs
        # now turn it into a dictionary
        headers = {x[0]: x[1] for x in response.getheaders()}
        debug(f'headers == {headers!r}')

        response_dict = self.make_response_dict(response)
        debug(f'response_dict == {response_dict!r}')

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict, headers

    @staticmethod
    def _make_headers(body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Dict[str, Any]:
        '''make request headers
        Automatically set Content-Type: application/json if needed

        Returns headers dictionary
        '''

        if headers is None:
            if body:
                # there is a body; provide content-type header
                return {'Content-Type': 'application/json'}
            return {}
        assert headers is not None      # this helps mypy
        if body:
            if 'Content-Type' not in headers:
                headers['Content-Type'] = 'application/json'
        return headers

    def post(self, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Dict[str, Any]:
        '''do a Redfish POST request
        Returns dictionary with response data
        May raise RedfishError
        '''

        assert self.redfish_obj is not None

        headers = self._make_headers(body, headers)

        debug(f'POST {url} body == {body!r} headers == {headers!r}')
        response = self.redfish_obj.post(url, body=body, headers=headers)
        debug(f'response.status == {self.format_http_status(response.status)}')

        response_dict = self.make_response_dict(response)
        debug(f'response_dict == {response_dict!r}')

        if response.status not in (http.HTTPStatus.OK, http.HTTPStatus.NO_CONTENT, http.HTTPStatus.ACCEPTED, http.HTTPStatus.CREATED):
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if response.status == http.HTTPStatus.ACCEPTED:
            # HTTP 202 Accepted; asynchronous task in server
            try:
                task_url = response_dict['@odata.id']
                task = RedfishTask(task_url, self)
                task.monitor()
            except KeyError:
                # well, we got an ACCEPTED without any Redfish task
                # we can't monitor that
                debug('job was accepted, but Redfish task id not found')

        debug(self.get_message(response_dict))
        return response_dict

    # def put(self, )

    # def head(self, )

    def patch(self, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Dict[str, Any]:
        '''do a Redfish PATCH request
        Returns dictionary with response data
        May raise RedfishError
        '''

        assert self.redfish_obj is not None

        headers = self._make_headers(body, headers)

        debug(f'PATCH {url} body={body!r} headers={headers!r}')
        response = self.redfish_obj.patch(url, body=body, headers=headers)
        debug(f'response.status == {self.format_http_status(response.status)}')

        response_dict = self.make_response_dict(response)
        debug(f'response_dict == {response_dict!r}')

        if response.status not in (http.HTTPStatus.OK, http.HTTPStatus.ACCEPTED, http.HTTPStatus.NO_CONTENT):
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict

    def delete(self, url: str) -> None:
        '''do a Redfish DELETE request'''

        debug(f'DELETE {url}')
        assert self.redfish_obj is not None
        self.redfish_obj.delete(url, None)

        # NOTE no response is being checked here

    @staticmethod
    def get_extended_error(response: Dict[str, Any]) -> str:
        '''get extended error message from Redfish response'''

        try:
            return RedfishClient.get_message(response['error'])

        except KeyError:
            return str(response)

    @staticmethod
    def get_message(response: Dict[str, Any]) -> str:
        '''get extended message from Redfish response'''

        try:
            extended_info = response['@Message.ExtendedInfo']

            if isinstance(extended_info, list):
                # join all messages in the list
                return '\n'.join([x['Message'] for x in extended_info]).rstrip()

            # it is a dict containing a single message
            return extended_info['Message']

        except KeyError:
            return ''

    def query_urls(self, path: str, url: str = '/redfish/v1') -> List[str]:
        '''Returns list of URLs for pseudo-path

        pseudo-path is something like:
            "Chassis:Members:ThermalSubsystem:Fans:Members"

        May raise RedfishError, KeyError for invalid path
        '''

        debug(f'path == {path!r}  url == {url}')

        if url is None:
            # how did this happen? Maybe bad Redfish vendor implementation?
            return []

        if not path:
            return [url, ]

        data = self.get(url)

        try:
            elem, path = path.split(':', maxsplit=1)
        except ValueError:
            # final path element
            elem = path
            path = ''

        if elem not in data:
            # this may happen when members do not have that subsystem
            return []

        try:
            urls = self._query_urls_item(data[elem], path)
            # make unique
            urls = list(dict.fromkeys(urls))
            urls = [x for x in urls if x is not None]
            return urls

        except KeyError as err:
            raise KeyError(f'invalid path: {path!r}') from err

    def _query_urls_item(self, data: Any, path: str) -> List[str]:
        '''helper function for query_urls()
        that deals with a single response item

        Raises KeyError for invalid path
        '''

        #pylint: disable=too-many-return-statements

        if isinstance(data, dict):
            if len(data) == 1 and '@odata.id' in data:
                # it is an object referral
                url = data['@odata.id']
                if url is None:
                    return []
                return self.query_urls(path, url)

            # it's a nested dict; walk the path
            try:
                elem, path = path.split(':', maxsplit=1)
            except ValueError:
                # final path element
                elem = path
                path = ''

            if elem not in data:
                # this may happen when members do not have that subsystem
                return []

            return self._query_urls_item(data[elem], path)

        if isinstance(data, list):
            urls = []
            for member in data:
                urls.extend(self._query_urls_item(member, path))
            return urls

        if isinstance(data, str) and data.startswith('/redfish/'):
            # it is an URL
            if path:
                # follow the URL
                return self.query_urls(path, data)
            # final path element: return the URL itself
            return [data, ]

        raise KeyError('invalid path')

    def load(self, path: str) -> List[Dict[str, Any]]:
        '''fetch Redfish data by pseudo-path

        pseudo-path is something like:
            "Chassis:Members:ThermalSubsystem:Fans:Members"

        Returns list of dictionaries (a list of Redfish objects)

        May raise RedfishError, KeyError for invalid path
        '''

        urls = self.query_urls(path)
        debug(f'path == {path!r}  urls == {json.dumps(urls, indent=2)}')

        objs = []
        for url in urls:
            objs.append(self.get(url))
        return objs



class RedfishTask:
    '''represents a Redfish task
    A task is an asynchronous server process

    (This class gets used by RedfishClient, usually one does not instantiate
    this class yourself)
    '''

    def __init__(self, url: str, client: RedfishClient) -> None:
        '''initialize instance'''

        self.url = url
        self.client = client
        self.state = ''                 # Running|Completed|... etc.
        self.percent = -1
        self.messages = []              # type: List[Dict[str, Any]]
        self.progress_visible = False   # is the progress meter visible?

    @staticmethod
    def format_json(d: Dict[str, Any]) -> str:
        '''Returns pretty-printed JSON string'''

        return json.dumps(d, indent=2)

    def monitor(self) -> None:
        '''wait for asynchronous task to complete

        This method prints messages to stdout

        May raise RedfishError
        '''

        self.state = ''
        self.percent = -1
        self.messages = []

        while True:
            response = self.client.get(self.url)
            self.handle_state(response)
            if not self.is_running():
                break

            time.sleep(0.8)     # seconds

        self.delete()

    S_RUNNING = ('New', 'Pending', 'Service', 'Starting', 'Stopping', 'Running', 'Cancelling', 'Verifying')
    S_END = ('Cancelled', 'Completed', 'Exception', 'Killed', 'Interrupted', 'Suspended')

    def is_running(self) -> bool:
        '''Returns True if the task is running
        May raise RedfishError for invalid state
        '''

        if self.state in RedfishTask.S_RUNNING or self.state.startswith('Downloading') or self.state.startswith('Update'):
            return True

        if self.state in RedfishTask.S_END:
            return False

        raise RedfishError(f'invalid task state {self.state!r}')

    def handle_state(self, response: Dict[str, Any]) -> None:
        '''update and handle the state of this asynchronous task

        This method prints messages to stdout

        May raise RedfishError
        '''

        task_state = response['TaskState']

        if task_state in RedfishTask.S_RUNNING:
            if task_state != self.state:
                self.state = task_state
                self.clear_progress()
                lower_state = self.state.lower()
                print(f'{lower_state} task')
            else:
                # task is running, still in same state
                try:
                    task_percent = response['PercentComplete']
                    self.show_progress(task_percent)
                except KeyError:
                    pass

        elif task_state.startswith('Downloading') or task_state.startswith('Update'):
            if task_state != self.state:
                self.state = task_state
                self.clear_progress()
                print(f'{self.state}')

        elif task_state in RedfishTask.S_END:
            if task_state != self.state:
                self.state = task_state

                # we may receive a final percentage?
                try:
                    task_percent = response['PercentComplete']
                    self.show_progress(task_percent)
                except KeyError:
                    pass
                self.finish_progress()

                # there may be additional messages;
                # display them
                try:
                    self.messages = response['Messages']
                    for msg_dict in self.messages:
                        try:
                            print(msg_dict['Message'])
                        except KeyError:
                            pass

                except KeyError:
                    pass

                lower_state = self.state.lower()
                print(f'task {lower_state}')

        else:
            raise RedfishError(f'invalid task state {task_state!r}')

    def show_progress(self, percent: int) -> None:
        '''display progress counter in percentage'''

        if percent != self.percent:
            if self.progress_visible:
                self.clear_progress()
            else:
                self.progress_visible = True

            self.percent = percent
            print(f'{self.percent}% complete', end='', flush=True)

    def clear_progress(self) -> None:
        '''clear progress display'''

        if self.progress_visible:
            # NOTE this print does not flush by itself
            print('\r                    \r', end='')

    def finish_progress(self) -> None:
        '''end the progress display'''

        if self.progress_visible:
            self.progress_visible = False
            print()

    def is_completed(self) -> bool:
        '''Returns True if the task is completed'''

        return self.state == 'Completed'

    def delete(self) -> None:
        '''delete this task (if necessary)

        The task is only deleted when it is 'Completed' with 'Severity: OK',
        or if it completed without any additional messages
        '''

        if self.is_completed():
            try:
                if not self.messages or self.messages[0]['Severity'] == 'OK':
                    # delete the task by URL
                    self.client.delete(self.url)

            except KeyError:
                pass


# EOB

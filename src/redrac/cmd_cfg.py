#
#   redrac cmd_cfg.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: cfg

Shows information about the remote management controller and the system
'''

import argparse

from typing import List, Dict, Any

from redrac import progress
from redrac import redfish_util
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.util import print_dict, print_data


def do_cfg(host: str, args: argparse.Namespace) -> None:
    '''get system configuration'''

    progress.start()

    if args.hostname:
        change_hostname(args.hostname, host, args)
        return

    data_list = redfish_util.load('Systems:Members', host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    cfg = cfg_from_data(data_list)
    print_dict(cfg)


def cfg_from_data(data_list: List[Dict[str, Any]]) -> Dict[str, Any]:
    '''filter Redfish data, we only wish to see certain keys
    Returns new cfg dictionary with system properties
    '''

    properties = ['HostName', 'Model', 'Manufacturer', 'SystemType',
                  'SerialNumber', 'AssetTag', 'ServiceTag', 'SKU', 'PartNumber', 'UUID',
                  'BiosVersion',
                  'PowerState', 'Status',
                  'ProcessorSummary', 'MemorySummary', 'TrustedModules']

    cfg = {}

    for member in data_list:
        for key in properties:
            try:
                if key not in cfg:
                    cfg[key] = member[key]
                    debug(f'cfg[{key!r}] == {cfg[key]!r}')
            except KeyError:
                pass

    cfg = redfish_util.strip_redfish_keys(cfg)
    return cfg


def change_hostname(hostname: str, host: str, args: argparse.Namespace) -> None:
    '''change the Systems:HostName property
    May raise RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            hostname_url = redfish_obj.query_urls('Managers:Members:EthernetInterfaces:Members')[0]
            debug(f'hostname_url == {hostname_url}')
        except (KeyError, IndexError) as err:
            raise RedfishError('Redfish URL to the Managers HostName not found') from err

        body = {'HostName': hostname}
        headers = {'If-Match': '*',
                   'Content-Type': 'application/json'}
        redfish_obj.patch(hostname_url, body, headers)


# EOB

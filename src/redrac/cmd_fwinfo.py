#
#   redrac cmd_fwinfo.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: fwinfo

Show loaded firmwares
'''

import argparse

from typing import List, Dict, Any

from redrac import progress
from redrac import redfish_util
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


class FirmwareInfo:
    '''represents firmware info'''

    def __init__(self, id_tag: str, version: str, software_id: str, release_date: str) -> None:
        '''initialize instance'''

        self.id_tag = id_tag
        self.version = version
        self.software_id = software_id
        self.release_date = release_date

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'FirmwareInfo':
        '''instantiate from data dictionary
        May raise KeyError
        '''

        # NOTE these keys are in the Redfish standard, so
        # NOTE the KeyError should not happen

        id_tag = data['Id']
        version = data['Version']
        software_id = data['SoftwareId']
        release_date = data['ReleaseDate']
        return FirmwareInfo(id_tag, version, software_id, release_date)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns dictionary'''

        return {'Id': self.id_tag,
                'Version': self.version,
                'SoftwareId': self.software_id,
                'ReleaseDate': self.release_date}

    def __repr__(self) -> str:
        '''Returns string'''

        return (f'FirmwareInfo(id_tag={self.id_tag}, version={self.version}, '
                f'software_id={self.software_id}, release_date={self.release_date})')


def do_fwinfo(host: str, args: argparse.Namespace) -> None:
    '''print firmware inventory'''

    progress.start()

    data_list = redfish_util.load('UpdateService:FirmwareInventory:Members', host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    fwinfo_list = fwinfo_from_data(data_list)

    # for a nicer display, show empty values as a minus
    for fwinfo in fwinfo_list:
        if not fwinfo.version:
            fwinfo.version = '-'
        if not fwinfo.software_id:
            fwinfo.software_id = '-'
        if not fwinfo.release_date:
            fwinfo.release_date = '-'

    id_tag_width = max(len(x.id_tag) for x in fwinfo_list)
    version_width = max(len(x.version) for x in fwinfo_list)
    software_id_width = max(len(x.software_id) for x in fwinfo_list)

    for x in fwinfo_list:
        print(f'{x.id_tag:<{id_tag_width}} {x.version:<{version_width}} {x.software_id:<{software_id_width}} {x.release_date}')


def fwinfo_from_data(data_list: List[Dict[str, Any]]) -> List[FirmwareInfo]:
    '''make FirmwareInfo from Redfish data
    Returns list of FirmwareInfo
    '''

    fwinfo_list = [FirmwareInfo.from_data(x) for x in data_list]

    # remove entries that do not have an id
    fwinfo_list = [x for x in fwinfo_list if x.id_tag]

    return fwinfo_list


# EOB

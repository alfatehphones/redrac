#
#   redrac cmd_thermals.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: thermals'''

import argparse

from typing import List, Dict, Tuple, Optional, Any

from redrac import progress
from redrac import redfish_util
from redrac.debug import debug
from redrac.redfishcode import RedfishClient
from redrac.hostconf import get_host
from redrac.thresholds import Thresholds
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


class Temperature:
    '''represents a temperature reading'''

    def __init__(self, name: str = '', reading: float = 0.0, unit: str = '',
                 thresholds: Optional[Thresholds] = None, sensor_url: str = '') -> None:
        '''initialize instance'''

        self.name = name
        self.reading = reading
        self.unit = unit                # e.g. 'C' for Celcius
        self.thresholds = thresholds
        self.sensor_url = sensor_url

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Temperature':
        '''instantiate from data dictionary'''

        try:
            name = data['Name']
        except KeyError:
            name = '(sensor)'

        try:
            # this makes 'Cel' become just 'C' (looking at you, DELL)
            unit = data['ReadingUnits'].upper()[0]
        except (KeyError, IndexError):
            unit = ''

        try:
            if isinstance(data['Reading'], list):
                reading = float(data['Reading'][0])
            else:
                reading = float(data['Reading'])

        except (ValueError, KeyError):
            reading = 0.0

        except TypeError:
            # invalid reading (probably: `None`)
            # therefore also reset the unit
            reading = 0.0
            unit = ''

        try:
            thresholds = Thresholds.from_data(data['Thresholds'])           # type: Optional[Thresholds]
        except KeyError:
            thresholds = None

        try:
            sensor_url = data['@odata.id']
        except KeyError:
            sensor_url = ''

        return Temperature(name, reading, unit, thresholds, sensor_url)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns instance as dictionary'''

        if self.thresholds is None:
            threshold_dict = {}
        else:
            threshold_dict = self.thresholds.as_dict()

        return {'Name': self.name,
                'Reading': self.reading,
                'ReadingUnits': self.unit,
                'Thresholds': threshold_dict}

    def __repr__(self) -> str:
        '''Returns string'''

        return f'Temperature(name={self.name!r}, reading={self.reading!r}, unit={self.unit!r}, thresholds={self.thresholds!r})'



def do_thermals(host: str, args: argparse.Namespace) -> None:
    '''do thermals command
    Show temperature readings
    '''

    progress.start()

    data_list = redfish_util.load('Chassis:Members:ThermalSubsystem:ThermalMetrics:TemperatureReadingsCelsius:DataSourceUri', host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    thermals = thermals_from_data(data_list)

    if not thermals:
        print('no temperature sensors found')
        return

    # sort by name
    thermals = sorted(thermals, key=lambda x: x.name)

    if args.warn or args.crit:
        # help out with not-so-clear usage
        args.threshold = True

    if args.threshold:
        change_threshold(thermals, host, args)
    else:
        # show thermals
        show_thermals(thermals, args)


def show_thermals(thermals: List[Temperature], args: argparse.Namespace) -> None:
    '''show thermals and the sensor's thresholds'''

    if args.name:
        # show single entry
        temperature_obj = thermal_by_name(args.name, thermals)
        thermals = [temperature_obj, ]

    width = max(len(x.name) for x in thermals)

    spacer = ''
    header = Thresholds.header()
    print(f'{spacer:<{width}} {spacer:>5} {spacer:>1}    {header}')
    for record in thermals:
        print(f'{record.name:<{width}} {record.reading:>5.1f} {record.unit:>1}    {record.thresholds}')


def thermals_from_data(data_list: List[Dict[str, Any]]) -> List[Temperature]:
    '''convert Redfish data to Temperature objects
    Returns list of Temperature instances
    '''

    thermal_list = []                   # type: List[Temperature]
    try:
        for data in data_list:
            thermal = Temperature.from_data(data)
            debug(f'thermal == {thermal!r}')
            thermal_list.append(thermal)

    except KeyError:
        pass

    return thermal_list


def change_threshold(thermals: List[Temperature], host: str, args: argparse.Namespace) -> None:
    '''set user thresholds'''

    if not args.name:
        raise ValueError('missing argument: --name=SENSOR')
    if not args.warn and not args.crit:
        raise ValueError('missing argument: --warning=LOWER:UPPER and/or --critical=LOWER:UPPER')

    temperature_obj = thermal_by_name(args.name, thermals)
    patch_thresholds(temperature_obj.sensor_url, host, args)

    # if we got here, all must be OK
    print('ok')


def thermal_by_name(name: str, thermals: List[Temperature]) -> Temperature:
    '''Returns Temperature instance that has `name`
    Raises KeyError if not found
    '''

    for t in thermals:
        if t.name.lower() == name.lower():
            return t

    raise KeyError(f'no such thermal sensor: {name!r}')


def patch_thresholds(sensor_url: str, host: str, args: argparse.Namespace) -> None:
    '''send PATCH request to set user thresholds
    May raise RedfishError, ValueError
    '''

    debug(f'sensor_url == {sensor_url}')
    if not sensor_url:
        raise ValueError('sensor URL not found')

    body = make_body_for_thresholds(args.warn, args.crit)
    debug(f'body == {body!r}')

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        _ = redfish_obj.patch(sensor_url, body=body)


def make_body_for_thresholds(arg_warn: Optional[str], arg_crit: Optional[str]) -> Dict[str, Any]:
    '''Returns request body for given thresholds
    May raise ValueError for invalid argument
    '''

    if not arg_warn and not arg_crit:
        raise ValueError('missing argument: --warning=LOWER:UPPER and/or --critical=LOWER:UPPER')

    body = {'Thresholds': {}}           # type: Dict[str, Any]

    thresh = Thresholds()
    if arg_warn:
        thresh.lower_caution, thresh.upper_caution = parse_arg_threshold(arg_warn)
        body['Thresholds']['LowerCaution'] = {'Reading': thresh.lower_caution}
        body['Thresholds']['UpperCaution'] = {'Reading': thresh.upper_caution}

    if arg_crit:
        thresh.lower_critical_user, thresh.upper_critical_user = parse_arg_threshold(arg_crit)
        body['Thresholds']['LowerCritical'] = {'Reading': thresh.lower_critical}
        body['Thresholds']['UpperCritical'] = {'Reading': thresh.upper_critical}

    debug(f'new thresholds == {thresh!r}')
    return body


def parse_arg_threshold(lower_upper: str) -> Tuple[Optional[float], Optional[float]]:
    '''Parse a threshold definition string
    The format of the string is:

        lower:upper

    lower and upper may be floats or ints, or  "-" (minus) to delete

    Returns tuple: lower, upper value
    May raise ValueError for invalid syntax
    '''

    try:
        s_lower, s_upper = lower_upper.split(':', maxsplit=1)
        if s_lower == '-' or not s_lower:
            f_lower = None              # type: Optional[float]
        else:
            f_lower = float(s_lower)

        if s_upper == '-' or not s_upper:
            f_upper = None              # type: Optional[float]
        else:
            f_upper = float(s_upper)

        return f_lower, f_upper

    except ValueError as err:
        raise ValueError(f'syntax error in threshold: {lower_upper!r}') from err


# EOB

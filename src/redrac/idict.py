#
#   idict.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''case-insensitive (but case-preserving) dictionary'''

from typing import Dict, Optional, Any


class idict(dict):
    '''case-insensitive (but case-preserving) dictionary
    keys must be of type string
    '''

    def __init__(self, d: Optional[Dict[str, Any]] = None, **kwargs: Any) -> None:
        '''initialize instance'''

        data = {}
        insensitive = {}

        if d is not None:
            for key, value in d.items():
                data[key] = value
                if isinstance(key, str):
                    insensitive[key.lower()] = key

        if kwargs:
            for key, value in kwargs.items():
                data[key] = value
                if isinstance(key, str):
                    insensitive[key.lower()] = key

        super().__init__(data)

        self.insensitive = insensitive

    def __getitem__(self, key: str) -> Any:
        '''Returns item by key
        May raise KeyError
        '''

        if isinstance(key, str):
            try:
                key = self.insensitive[key.lower()]
            except KeyError:
                # let the original __getitem__() bomb out on the given key
                pass
        return super().__getitem__(key)

    def __setitem__(self, key: str, value: Any) -> None:
        '''set item by key'''

        super().__setitem__(key, value)

        if isinstance(key, str):
            try:
                previous_key = self.insensitive[key.lower()]
                if previous_key != key:
                    # update case-preserving key
                    self.insensitive[key.lower()] = key
                    try:
                        # delete item for key in a different casing
                        super().__delitem__(previous_key)
                    except KeyError:
                        pass

            except KeyError:
                # unknown key; insert new key
                self.insensitive[key.lower()] = key

    def __delitem__(self, key: str) -> None:
        '''delete item by key'''

        if isinstance(key, str):
            try:
                key = self.insensitive[key.lower()]
                del self.insensitive[key.lower()]
            except KeyError:
                # let the original __delitem__() bomb out on the given key
                pass
        super().__delitem__(key)

    def __contains__(self, key: object) -> bool:
        '''Returns True if dictionary key exists'''

        if isinstance(key, str):
            try:
                key = self.insensitive[key.lower()]
            except KeyError:
                # let the original __contains__() bomb out on the given key
                pass
        return super().__contains__(key)


# EOB

#
#   redrac cmd_led.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: led'''

import argparse

from typing import List, Dict, Tuple, Any

from redrac import progress
from redrac.debug import debug
from redrac import redfish_util
from redrac.redfishcode import RedfishClient
from redrac.hostconf import get_host
from redrac.util import print_dict
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_led(host: str, args: argparse.Namespace) -> None:
    '''do LED command'''

    if args.on and args.off:
        raise ValueError('usage: led [--on|--off]')

    progress.start()

    if args.on:
        redfish_led_control(host, args, 'on')

    elif args.off:
        redfish_led_control(host, args, 'off')

    else:
        show_led_state(host, args)


def show_led_state(host: str, args: argparse.Namespace) -> None:
    '''show the indicator LED state'''

    data_list = redfish_util.load('Chassis:Members', host, args)
    state = led_state_from_data(data_list)

    if args.json:
        d = {'LedState': state}
        print_dict(d, as_json=True)
    else:
        print(f'LED: {state}')


def led_state_from_data(data_list: List[Dict[str, Any]]) -> str:
    '''get indicator LED state from Redfish data
    Returns LED state as string

    The Redfish property is `LocationIndicatorActive` (boolean)
    Earlier versions of the standard use `IndicatorLED` (string)
    We check both, and convert to string
    Due to these different versions of the standard, the returned string
    may be one of: On|Off|Lit|Blinking|Unknown
    '''

    # the LED state should be present in a Chassis Member

    for member in data_list:
        try:
            state = member['LocationIndicatorActive']
            debug(f'LocationIndicatorActive == {state}')
            # return the first found state
            if state:
                return 'On'
            return 'Off'

        except KeyError:
            try:
                svalue = member['IndicatorLED']
                # NOTE the value may also be `[None,]` (which is not OK)
                if isinstance(svalue, str):
                    debug(f'IndicatorLED == {svalue!r}')
                    # return the first found state
                    return svalue

            except KeyError:
                pass

    debug('LocationIndicatorActive / IndicatorLED not found')
    return 'unknown'


def redfish_led_control(host: str, args: argparse.Namespace, on_off: str) -> None:
    '''turn indicator LED on or off

    May raise ValueError (if invalid led command)
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        led_url, key, etag = find_led_url(redfish_obj)

        if key == 'LocationIndicatorActive':
            bvalue = on_off == 'on'
            body = {key: bvalue}        # type: Dict[str, Any]

        elif key == 'IndicatorLED':
            if on_off == 'on':
                svalue = 'Lit'
            elif on_off == 'off':
                svalue = 'Off'
            else:
                raise ValueError(f'bug: on_off == {on_off!r}')

            body = {key: svalue}

        else:
            raise ValueError(f'bug: key == {key!r}')

        headers = {'Content-Type': 'application/json'}
        # set etag (if any)
        if etag:
            headers['If-Match'] = etag

        # change the LED state using a PATCH request
        redfish_obj.patch(led_url, body=body, headers=headers)

    # finally, show the current LED state
    # this will re-connect to the controller (which is slow, ah well)
    show_led_state(host, args)


def find_led_url(redfish_obj: RedfishClient) -> Tuple[str, str, str]:
    '''Returns tuple: (URL to the LED indicator, property name, etag)

    The returned property name may be either
    'LocationIndicatorActive' or 'IndicatorLED',
    depending on the exact version of Redfish standard
    that the remote management controller implements

    May raise KeyError if the URL can not be found
    '''

    urls = redfish_obj.query_urls('Chassis:Members')
    for url in urls:
        data = redfish_obj.get(url)

        try:
            etag = data['@odata.etag']
        except KeyError:
            etag = ''

        try:
            _ = data['LocationIndicatorActive']
            return url, 'LocationIndicatorActive', etag

        except KeyError:
            try:
                svalue = data['IndicatorLED']
                # NOTE the value may also be `[None,]` (which is not OK)
                if isinstance(svalue, str):
                    return url, 'IndicatorLED', etag

            except KeyError:
                continue

    debug('URL to LocationIndicatorActive / IndicatorLED not found')
    raise KeyError('URL to indicator LED not found')


# EOB

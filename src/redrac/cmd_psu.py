#
#   redrac cmd_psu.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: psu

Shows information about the power supplies
'''

import argparse

from typing import List, Dict, Any

from redrac import progress
from redrac.debug import debug
from redrac.redfishcode import RedfishClient
from redrac.hostconf import get_host
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


class PSU:
    '''represents a power supply'''

    def __init__(self, name: str, health: str, hotplug: bool = False, redundancy_type: str = '') -> None:
        '''initialize instance'''

        self.name = name
        self.health = health
        self.hotplug = hotplug
        self.redundancy_type = redundancy_type

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'PSU':
        '''instantiate from data dictionary'''

        try:
            name = data['Name']
        except KeyError:
            name = '(psu)'

        try:
            health = data['Status']['Health']

            # if the PSU is not enabled, then the health can not be 'OK' AFAIC
            if health.lower() == 'ok' and data['Status']['State'] != 'Enabled':
                health = 'Warning'

        except KeyError:
            health = 'unknown'

        try:
            hotplug = data['HotPluggable']
        except KeyError:
            hotplug = False

        try:
            redundancy_type = data['RedundancyType']
        except KeyError:
            redundancy_type = ''

        return PSU(name, health, hotplug, redundancy_type)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Name': self.name,
                'Health': self.health,
                'HotPluggable': self.hotplug,
                'RedundancyType': self.redundancy_type}

    def __repr__(self) -> str:
        '''Returns string'''

        return f'PSU(name={self.name!r}, health={self.health!r}, hotplug={self.hotplug!r}, redundancy_type={self.redundancy_type!r})'



def do_psu(host: str, args: argparse.Namespace) -> None:
    '''show PSU information'''

    progress.start()

    data_list = load_psu(host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    psu_list = [PSU.from_data(x) for x in data_list]
    if not psu_list:
        print('power supplies not found')
        return

    width = max(len(x.name) for x in psu_list)
    health_width = max(len(x.health) for x in psu_list)

    for psu in psu_list:
        if psu.hotplug:
            hotplug = 'hotplug'
        else:
            hotplug = ''
        print(f'{psu.name:<{width}}  health: {psu.health:<{health_width}}  {hotplug:<7}  {psu.redundancy_type}')


def load_psu(host: str, args: argparse.Namespace) -> List[Dict[str, Any]]:
    '''get PSU data from remote controller

    Returns list of Redfish objects
    May raise RedfishError
    '''

    # a system may have multiple chassis members, where one chassis member
    # has redundant PSUs, while another chassis member has non-redundant PSUs
    # (however unlikely)
    # Also note, if there are multiple distinct redundancy groups, then we lose
    # that information because we simply flatten the PSU list

    psu_list = []

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        data_list = redfish_obj.load('Chassis:Members:PowerSubsystem')
        for power_subsystem in data_list:
            try:
                for redundancy_obj in power_subsystem['PowerSupplyRedundancy']:
                    redundant_psu_list = _get_redundant_psu_list(redfish_obj, redundancy_obj)
                    psu_list.extend(redundant_psu_list)

            except KeyError:
                # no redundant power supplies in this chassis member
                # get list of present PSUs
                plain_psu_list = _get_plain_psu_list(redfish_obj, power_subsystem)
                psu_list.extend(plain_psu_list)

    return psu_list


def _get_redundant_psu_list(redfish_obj: RedfishClient, redundancy_obj: Dict[str, Any]) -> List[Dict[str, Any]]:
    '''Returns list of redundant PSU data
    May raise RedfishError, or KeyError if there are no redundant PSUs
    '''

    redundancy_type = redundancy_obj['RedundancyType']

    redundant_psu_urls = [x['@odata.id'] for x in redundancy_obj['RedundancyGroup']]
    if not redundant_psu_urls:
        debug('redundant PSUs are possible, but none are present')
        return []

    debug('we have redundant PSUs')

    psu_list = []

    for url in redundant_psu_urls:
        psu_data = redfish_obj.get(url)
        # patch the redundancy type into each PSU data object
        psu_data['RedundancyType'] = redundancy_type
        psu_list.append(psu_data)

    return psu_list


def _get_plain_psu_list(redfish_obj: RedfishClient, power_subsystem: Dict[str, Any]) -> List[Dict[str, Any]]:
    '''Returns list of (non-redundant) PSU data
    May raise RedfishError
    '''

    debug('get list of non-redundant PSUs')

    psu_list = []

    powersupplies_url = power_subsystem['PowerSupplies']['@odata.id']
    powersupplies_data = redfish_obj.get(powersupplies_url)
    psu_urls = [x['@odata.id'] for x in powersupplies_data['Members']]
    for url in psu_urls:
        psu_data = redfish_obj.get(url)
        psu_list.append(psu_data)

    return psu_list


# EOB

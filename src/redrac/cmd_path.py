#
#   redrac cmd_path.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: path

Get Redfish URL(s) for given pseudo-path
or import/export Redfish object via pseudo-path
'''

import argparse
import json
import os

from typing import List, Dict, Union, Any

from redrac import progress
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.redfish_util import path_match_urls
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_path(host: str, args: argparse.Namespace) -> None:
    '''get Redfish URL(s) for given pseudo-path'''

    host, username, password, cafile = get_host(host, args).as_tuple()

    if args.import_file:
        # load settings from file
        with open(args.import_file, encoding='utf-8') as fh:
            try:
                settings = json.load(fh)
            except json.decoder.JSONDecodeError as err:
                short_filename = os.path.basename(args.import_file)
                raise ValueError(f'{short_filename}: {err}') from err

    progress.start()

    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            urls = redfish_obj.query_urls(args.path)
            debug(f'urls == {urls!r}')
        except (KeyError, IndexError) as err:
            raise RedfishError('invalid path; Redfish URL not found') from err

        if not urls:
            raise RedfishError('invalid path; Redfish URL not found')

        if args.match:
            # select URL by path-match pattern
            urls = [path_match_urls(redfish_obj, args.match, urls), ]

        if args.export:
            if len(urls) > 1:
                data_list = []
                for url in urls:
                    data = redfish_obj.get(url)
                    data_list.append(data)
                print_data(data_list, as_json=True)
            else:
                data = redfish_obj.get(urls[0])
                print_data(data, as_json=True)
            return

        if args.import_file:
            try:
                apply_settings(redfish_obj, urls, settings)
            except ValueError as err:
                short_filename = os.path.basename(args.import_file)
                raise ValueError(f'{short_filename}: invalid JSON; {err}') from err
            return

    # show URLs
    if args.json:
        print(json.dumps(urls))
    else:
        for url in urls:
            print(url)


def apply_settings(redfish_obj: RedfishClient, urls: List[str], settings: Union[Dict[str, Any], List[Dict[str, Any]]]) -> None:
    '''apply settings to Redfish URLs
    May raise RedfishError, ValueError for invalid settings
    '''

    if len(urls) == 1:
        apply_settings_to(redfish_obj, urls[0], settings)

    elif isinstance(settings, list):
        map_settings_to_urls(redfish_obj, urls, settings)

    else:
        # each url gets the same settings
        for url in urls:
            apply_settings_to(redfish_obj, url, settings)


def apply_settings_to(redfish_obj: RedfishClient, url: str, settings: Union[Dict[str, Any], List[Dict[str, Any]]]) -> None:
    '''apply settings to Redfish URL via PATCH request
    May raise RedfishError, ValueError for invalid settings
    '''

    if not isinstance(settings, dict):
        raise ValueError('expected a Redfish object')

    body = settings
    headers = {'If-Match': '*',
               'Content-Type': 'application/json'}
    redfish_obj.patch(url, body, headers)


def map_settings_to_urls(redfish_obj: RedfishClient, urls: List[str], settings: Union[Dict[str, Any], List[Dict[str, Any]]]) -> None:
    '''map list of settings to list of Redfish URLs
    May raise RedfishError, ValueError for invalid settings
    '''

    if not isinstance(settings, list):
        raise ValueError('expected a list of Redfish objects')

    if len(urls) != len(settings):
        if len(settings) == 1:
            s_objects = 'one Redfish object'
        else:
            s_objects = f'{len(settings)} Redfish objects'
        if len(urls) == 1:
            s_urls = 'one URL'
        else:
            s_urls = f'{len(urls)} URLs'
        raise ValueError(f'expected {s_objects} (for {s_urls})')

    for idx, url in enumerate(urls):
        apply_settings_to(redfish_obj, url, settings[idx])


# EOB

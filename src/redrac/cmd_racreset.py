#
#   redrac cmd_racreset.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: racreset

Restarts the remote management controller
'''

import argparse

from typing import List

from redrac import progress
from redrac.debug import debug
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.hostconf import get_host
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_racreset(host: str, args: argparse.Namespace) -> None:
    '''handle racreset command

    racreset --list : show available reset types
    racreset -f state : reset the remote controller
    '''

    progress.start()

    if args.list:
        show_reset_types(host, args)

    elif args.force:
        apply_reset_type(host, args, args.force)

    elif args.factory_reset:
        factoryreset_manager(host, args)

    else:
        raise ValueError('usage: racreset --list | --force STATE | --factory-reset')


def show_reset_types(host: str, args: argparse.Namespace) -> None:
    '''show available reset types

    Get the Redfish Manager ResetType(s) from the remote controller
    and show them
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    reset_types = obtain_reset_types(host, username, password, cafile)
    for reset_type in sorted(reset_types):
        print(f'  {reset_type}')


def obtain_reset_types(host: str, username: str, password: str, cafile: str = '') -> List[str]:
    '''Returns list of Manager ResetType(s) that the remote controller supports

    May raise RedfishError if no ResetTypes were found
    '''

    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        data_list = redfish_obj.load('Managers:Members')
        for data in data_list:
            try:
                reset_types = data['Actions']['#Manager.Reset']['ResetType@Redfish.AllowableValues']
                return reset_types
            except KeyError:
                pass

    raise RedfishError('no reset types found')


def apply_reset_type(host: str, args: argparse.Namespace, state: str) -> None:
    '''give a manager reset command'''

    host, username, password, cafile = get_host(host, args).as_tuple()
    redfish_manager_reset(host, username, password, cafile, state)
    print(f'{host}: ok')


def redfish_manager_reset(host: str, username: str, password: str, cafile: str, state: str) -> None:
    '''reset the manager; the remote access controller
    'state' is the ResetType (may be in lowercase)
    like: 'gracefulrestart', 'forcerestart', etc.

    May raise RedfishError
    '''

    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        data_list = redfish_obj.load('Managers:Members')
        for data in data_list:
            try:
                # get the available ResetTypes
                # we may provide the reset type in lowercase
                # when we POST the ResetType, it must be spelled as it appears in the available ResetTypes
                # (which is usually upper camelcase)
                available_reset_types = data['Actions']['#Manager.Reset']['ResetType@Redfish.AllowableValues']
                lower_reset_types = [x.lower() for x in available_reset_types]

                if state.lower() not in lower_reset_types:
                    raise ValueError(f'reset type not available: {state!r}')

                # get the ResetType in correct spelling  (upper camelcase)
                reset_types_map = {x.lower(): x for x in available_reset_types}
                post_reset_type = reset_types_map[state.lower()]

                # reset the system!
                post_body = {'ResetType': post_reset_type}
                reset_url = data['Actions']['#Manager.Reset']['target']
                debug(f'result_url == {reset_url!r}')
                redfish_obj.post(reset_url, body=post_body)
                return

            except KeyError:
                pass

    raise RedfishError('no reset types found')


def factoryreset_manager(host: str, args: argparse.Namespace) -> None:
    '''perform factoryreset of remote management controller using Redfish'''

    did_reset = 0

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        print('resetting remote management controller to factory defaults')

        # this is commented because we just want 'ResetAll' to be implemented
        #reset_types = data['Actions']['#Manager.ResetToDefaults']['ResetType@Redfish.AllowableValues']

        urls = redfish_obj.query_urls('Managers:Members:Actions:#Manager.ResetToDefaults:target')
        for url in urls:
            # invoke the reset action
            reset_response = redfish_obj.post(url, body={'ResetType': 'ResetAll'})

            # print messages (if any)
            msg = RedfishClient.get_message(reset_response).rstrip()
            if msg:
                print(msg)

            did_reset += 1

    if not did_reset:
        raise KeyError('redfish Manager.ResetToDefaults action not found in remote management controller')


# EOB

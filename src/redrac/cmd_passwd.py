#
#   redrac cmd_passwd.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: passwd

Change admin password of the remote management controller
'''

import argparse
import getpass

from typing import Dict, Tuple, Any

from redrac import progress
from redrac.debug import debug
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.hostconf import get_host
from redrac.print import print                                      # pylint: disable=redefined-builtin


def read_password_from_file(filename: str) -> str:
    '''Returns password (in plaintext)
    May raise OSError, ValueError if invalid password
    '''

    with open(filename, encoding='utf-8') as fh:
        new_password = fh.readline().rstrip()

    debug(f'new_password == {new_password!r}')

    if not new_password:
        raise ValueError(f'{filename} contains empty line')

    return new_password


def get_interactive_new_password() -> str:
    '''ask user for new password twice
    Returns new password (in plaintext)

    May raise ValueError if invalid password
    '''

    password1 = getpass.getpass('Enter new password: ')
    debug(f'password1 == {password1!r}')
    if not password1:
        raise ValueError('invalid password')

    password2 = getpass.getpass('Enter it again (for verification): ')
    debug(f'password2 == {password2!r}')

    if password1 != password2:
        raise ValueError('passwords do not match')

    return password1


def do_passwd(host: str, args: argparse.Namespace) -> None:
    '''change password command

    args.file may be a plaintext file containing the new password (for automation)

    May raise RedfishError, OSError, ValueError
    '''

    if args.file:
        new_password = read_password_from_file(args.file)
    else:
        new_password = ''

    progress.start()

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        account_url, account = find_account(redfish_obj, username)
        debug(f'account_url == {account_url}')
        debug(f'account == {account!r}')

        if not new_password:
            new_password = get_interactive_new_password()

        validate_password_for_account_svc(redfish_obj, new_password)

        redfish_change_password(redfish_obj, account_url, account, new_password)

    print('password changed')


def find_account(redfish_obj: RedfishClient, username: str) -> Tuple[str, Dict[str, Any]]:
    '''find account data for username

    Returns tuple: account_url, account_dict data
    May raise RedfishError, KeyError if not found
    '''

    data_list = redfish_obj.load('AccountService:Accounts:Members')
    for data in data_list:
        if data['UserName'] == username:
            account_url = data['@odata.id']
            return account_url, data

    raise KeyError(f'account {username!r} not found')


def validate_password_for_account_svc(redfish_obj: RedfishClient, new_password: str) -> None:
    '''check password constraints
    The password constraints may be (or may not be) present in the AccountService

    May raise RedfishError, ValueError for invalid password
    '''

    try:
        account_svc = redfish_obj.load('AccountService')[0]
    except (KeyError, IndexError) as err:
        raise RedfishError('failed to load Redfish AccountService') from err

    try:
        max_password_length = int(account_svc['MaxPasswordLength'])
    except (ValueError, KeyError):
        pass
    else:
        if len(new_password) > max_password_length:
            raise ValueError(f'password is too long (max {max_password_length} characters)')

    try:
        min_password_length = int(account_svc['MinPasswordLength'])
    except (ValueError, KeyError):
        pass
    else:
        if len(new_password) < min_password_length:
            raise ValueError(f'password is too short (minimum {min_password_length} characters)')


def redfish_change_password(redfish_obj: RedfishClient, account_url: str, account: Dict[str, Any], new_password: str) -> None:
    '''change password via HTTP PATCH request

    May raise RedfishError
    '''

    body = {'Password': new_password}
    try:
        headers = {'If-Match': account['@odata.etag']}
    except KeyError:
        headers = None

    response = redfish_obj.patch(account_url, body=body, headers=headers)

    # print messages (if any)
    msg = redfish_obj.get_message(response).rstrip()
    if msg:
        print(msg)


# EOB

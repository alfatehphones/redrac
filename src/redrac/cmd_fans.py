#
#   redrac cmd_fans.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: fans

Shows information about the fans
'''

import argparse

from typing import List, Dict, Any

from redrac import progress
from redrac import redfish_util
from redrac.debug import debug
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


class Fan:
    '''represents a fan'''

    def __init__(self, name: str, health: str, hotplug: bool = False) -> None:
        '''initialize instance'''

        self.name = name
        self.health = health
        self.hotplug = hotplug

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Fan':
        '''instantiate from data dictionary'''

        try:
            name = data['Name']
        except KeyError:
            name = '(fan)'

        try:
            health = data['Status']['Health']

            # if the fan is not enabled, then the health can not be 'OK' AFAIC
            if health.lower() == 'ok' and data['Status']['State'] != 'Enabled':
                health = 'Warning'

        except KeyError:
            health = 'unknown'

        try:
            hotplug = data['HotPluggable']
        except KeyError:
            hotplug = False

        return Fan(name, health, hotplug)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Name': self.name,
                'Health': self.health,
                'HotPluggable': self.hotplug}

    def __repr__(self) -> str:
        '''Returns string'''

        return f'Fan(name={self.name!r}, health={self.health!r}, hotplug={self.hotplug})'



def do_fans(host: str, args: argparse.Namespace) -> None:
    '''show fan information'''

    progress.start()

    data_list = redfish_util.load('Chassis:Members:ThermalSubsystem:Fans:Members', host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    fan_list = fans_from_data(data_list)

    if not fan_list:
        print('no fans present')
        return

    width = max(len(x.name) for x in fan_list)
    health_width = max(len(x.health) for x in fan_list)

    for fan in fan_list:
        if fan.hotplug:
            hotplug = 'hotplug'
        else:
            hotplug = ''
        print(f'{fan.name:<{width}}  health: {fan.health:<{health_width}}  {hotplug}')


def fans_from_data(data_list: List[Dict[str, Any]]) -> List[Fan]:
    '''convert Redfish data to Fan objects
    Returns list of Fan instances
    '''

    fan_list = []                       # type: List[Fan]
    try:
        for data in data_list:
            fan = Fan.from_data(data)
            debug(f'fan == {fan!r}')
            fan_list.append(fan)

    except KeyError:
        pass

    return fan_list


# EOB

#
#   redrac cmd_hwinfo.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: hwinfo

Show hardware information
'''

import argparse

from typing import List, Dict, Any

from redrac import progress
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient
from redrac.util import print_dict, print_data


def do_hwinfo(host: str, args: argparse.Namespace) -> None:
    '''print hardware inventory'''

    progress.start()

    data = load_hwinfo_data(host, args)

    if args.json:
        print_data(data, as_json=True)
        return

    hwinfo = hwinfo_from_data(data)
    print_dict(hwinfo)


def load_hwinfo_data(host: str, args: argparse.Namespace) -> List[Dict[str, Any]]:
    '''fetch Redfish data with hardware information
    Returns list of Redfish objects
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        chassis_members = redfish_obj.load('Chassis:Members')

        # expand various subsections
        for data in chassis_members:
            if 'Memory' in data:
                data['Memory']['Members'] = redfish_obj.load('Chassis:Members:Memory:Members')
            if 'Links' in data:
                if 'ManagedBy' in data['Links']:
                    data['Links']['ManagedBy'] = redfish_obj.load('Chassis:Members:Links:ManagedBy')
                if 'Processors' in data['Links']:
                    data['Links']['Processors'] = redfish_obj.load('Chassis:Members:Links:Processors')
                if 'Drives' in data['Links']:
                    data['Links']['Drives'] = redfish_obj.load('Chassis:Members:Links:Drives')
                if 'PCIeDevices' in data['Links']:
                    data['Links']['PCIeDevices'] = redfish_obj.load('Chassis:Members:Links:PCIeDevices')

    return chassis_members


def hwinfo_from_data(chassis_members: List[Dict[str, Any]]) -> Dict[str, Any]:
    '''extract relevant hardware part info from Redfish chassis members data
    Returns new dictionary containing hardware information
    '''

    hwinfo = get_chassis_info(chassis_members)

    # the 'manager' is the remote management controller
    manager = get_manager_info(chassis_members)
    if manager:
        hwinfo['Manager'] = manager

    hwinfo['CPU'] = get_cpu_info(chassis_members)
    hwinfo['Memory'] = get_memory_info(chassis_members)
    hwinfo['Disk'] = get_disk_info(chassis_members)
    hwinfo['Devices'] = get_pci_devices_info(chassis_members)
    return hwinfo


def get_chassis_info(chassis_members: List[Dict[str, Any]]) -> Dict[str, Any]:
    '''etxract (pre-defined) chassis keys from Redfish data
    Returns dictionary
    '''

    chassis_info = {}

    pod_keys = ('Manufacturer', 'Name', 'Model', 'ChassisType', 'SerialNumber', 'SKU', 'AssetTag')

    for member in chassis_members:
        for key in pod_keys:
            try:
                # NOTE this causes a 'preference' for the first Chassis Member
                # NOTE this is typically OK, but not sure whether this is _always_ OK ...?
                if key not in chassis_info:
                    chassis_info[key] = member[key]
            except KeyError:
                pass

    return chassis_info


def get_manager_info(chassis_members: List[Dict[str, Any]]) -> str:
    '''Returns manager string'''

    manager = ''

    for member in chassis_members:
        try:
            for managed_by in member['Links']['ManagedBy']:
                try:
                    model = managed_by['Model']
                    if model:
                        # return the first manager that is found
                        return model

                except KeyError:
                    pass

        except KeyError:
            pass

    return manager


def get_cpu_info(chassis_members: List[Dict[str, Any]]) -> List[str]:
    '''extract CPU information from chassis members
    Returns list of strings describing the CPUs
    '''

    cpu_info = []

    already_seen = set([])

    for member in chassis_members:
        try:
            for cpu in member['Links']['Processors']:
                try:
                    # check whether this part Id has already been seen
                    # In Redfish there may be multiple member paths
                    # showing the same hardware
                    part_id = cpu['Id']
                    if part_id in already_seen:
                        continue

                    cpu_info.append(cpu['Model'])
                    already_seen.add(part_id)
                except KeyError:
                    pass

        except KeyError:
            # no CPUs in this chassis member
            pass

    return cpu_info


def get_memory_info(chassis_members: List[Dict[str, Any]]) -> List[str]:
    '''extract memory information from chassis members
    Returns list of strings describing the memory DIMMs
    '''

    memory_info = []

    dimm_keys = ('Name', 'CapacityMiB', 'MemoryDeviceType', 'OperatingSpeedMhz', 'Manufacturer')

    already_seen = set([])

    for member in chassis_members:                                  # pylint: disable=too-many-nested-blocks
        try:
            for dimm in member['Memory']['Members']:
                try:
                    # check whether this part Id has already been seen
                    # In Redfish there may be multiple member paths
                    # showing the same hardware
                    part_id = dimm['Id']
                    if part_id in already_seen:
                        continue

                    # make a single line with DIMM information
                    dimm_elements = []          # type: List[str]
                    for key in dimm_keys:
                        try:
                            if dimm[key]:
                                dimm_elements.append(str(dimm[key]))
                        except KeyError:
                            pass

                    if dimm_elements:
                        dimm_str = ' '.join(dimm_elements)
                        memory_info.append(dimm_str)
                        already_seen.add(part_id)

                except KeyError:
                    pass

        except KeyError:
            # no memory in this chassis member
            pass

    return memory_info


def get_disk_info(chassis_members: List[Dict[str, Any]]) -> List[str]:
    '''extract disk drive information from chassis members
    Returns list of strings describing the disks
    '''

    disk_info = []

    already_seen = set([])

    for member in chassis_members:
        try:
            for disk in member['Links']['Drives']:
                try:
                    # check whether this part Id has already been seen
                    # In Redfish there may be multiple member paths
                    # showing the same hardware
                    part_id = disk['Id']
                    if part_id in already_seen:
                        continue

                    # make single line with disk information
                    disk_elements = []      # type: List[str]
                    size = disk['CapacityBytes']
                    if size >= 10**12:
                        size_str = f'{size / 10**12:.1f} TB'
                    else:
                        size_str = f'{int(size / 10**9)} GB'
                    disk_elements.append(size_str)
                    disk_elements.append(disk['Manufacturer'])
                    try:
                        disk_elements.append(disk['Protocol'])
                    except KeyError:
                        pass
                    disk_elements.append(disk['MediaType'])
                    disk_elements.append(disk['Model'])
                    disk_elements.append(disk['Name'])
                    disk_str = ' '.join(disk_elements)
                    disk_info.append(disk_str)
                    already_seen.add(part_id)

                except KeyError:
                    pass

        except KeyError:
            # no disk drives in chassis member
            pass

    return disk_info


def get_pci_devices_info(chassis_members: List[Dict[str, Any]]) -> List[str]:
    '''extract PCIe device information from chassis members
    Returns list of strings describing PCIe devices
    '''

    pci_devices = []

    already_seen = set([])

    for member in chassis_members:
        try:
            for device in member['Links']['PCIeDevices']:
                try:
                    # check whether this part Id has already been seen
                    # In Redfish there may be multiple member paths
                    # showing the same hardware
                    part_id = device['Id']
                    if part_id in already_seen:
                        continue

                    try:
                        manufacturer = device['Manufacturer']
                    except KeyError:
                        manufacturer = ''

                    name = device['Name']
                    if name == 'Adapter' or not name:
                        continue

                    if manufacturer and manufacturer not in name:
                        device_str = manufacturer + ' ' + name
                    else:
                        device_str = name

                    pci_devices.append(device_str)
                    already_seen.add(part_id)

                except KeyError:
                    pass

        except KeyError:
            # no PCIe devices in chassis member
            pass

    return pci_devices


# EOB
